#!/bin/bash

BASE=/home/velo/cernbox/Production/gitlab

# Update git repo
cd ${BASE}
git pull

# Copy glue scripts to template folder
cp -u ${BASE}/tile_glueing/gluepatterns.py ${BASE}/template_module/tile_glueing/glue_files/
cp -u ${BASE}/tile_glueing/geometry.py ${BASE}/template_module/tile_glueing/glue_files/
cp -u ${BASE}/tile_glueing/visualization.py ${BASE}/template_module/tile_glueing/glue_files/
 
cp -u ${BASE}/tile_glueing/hybrids_gluepatterns.py ${BASE}/template_module/hybrid_glueing/glue_files/
cp -u ${BASE}/tile_glueing/geometry.py ${BASE}/template_module/hybrid_glueing/glue_files/
cp -u ${BASE}/tile_glueing/visualization.py ${BASE}/template_module/hybrid_glueing/glue_files/

