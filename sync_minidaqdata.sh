#!/bin/bash
  
# Check for just one argument
if [ $# -ne 1 ]
then
    echo "Please enter the name of the module you wish to create"
    exit
fi

# Use argment to make directory names
MOD=$1

# Location of module data directory on this machine
LOCALDIR=/home/velo/cernbox/Production/${MOD}/electrical_tests

# Location with module data on minidaq
MINIDAQDIR=/home/velo/Velo_MiniDAQ/VeloPix_testing/${MOD}

# Check that the directory exists here
if [ ! -d ${LOCALDIR} ]
then
    echo "Local module directory does not exist, exiting"
    echo ${LOCALDIR}
    exit
fi

# Check that the remote directory exists
if [ ! 'ssh velo@minidaq test -d ${MINIDAQDIR}' ]
then echo "Remote module directory does not exist, exiting"
    exit
else
  ssh velo@minidaq ls -l ${MINIDAQDIR}
fi

rsync -av velo@minidaq:${MINIDAQDIR}/ ${LOCALDIR}
