#!/bin/bash
  
# Define location of cernbox on this machine
CERNBOX=/home/velo/cernbox/Production

# Define location of template folder
TEMPLATE=${CERNBOX}/gitlab/template_module

# Check for just one argument
if [ $# -ne 1 ]
then
    echo "Please enter the name of the module you wish to create"
    exit
fi

# Use argment to make directory names
MOD=$1
echo "Setting up directories for module $MOD"
BASE_DIR=${CERNBOX}/${MOD}

# Check if base directory already exists
if [ -d ${BASE_DIR} ]
then 
    echo "Directory $BASE_DIR already exists, exiting"
    exit
fi

# Create directory
cp -r ${TEMPLATE} ${CERNBOX}/${MOD}
echo "- created folders under ${CERNBOX}/${MOD}"
