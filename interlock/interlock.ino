/*
 * Inputs ADC Value from Thermistor and outputs Temperature in Celsius
 *  requires: include <math.h>
 * Utilizes the Steinhart-Hart Thermistor Equation:
 *    Temperature in Kelvin = 1 / {A + B[ln(R)] + C[ln(R)]3}
 *    where A = 0.001129148, B = 0.000234125 and C = 8.76741E-08
 *
 * These coefficients seem to work fairly universally, which is a bit of a
 * surprise.
 *
 * Schematic:
 *   [Ground] -- [10k-pad-resistor] -- | -- [thermistor] --[Vcc (5 or 3.3v)]
 *                                               |
 *                                          Analog Pin 0
 *
 * In case it isn't obvious (as it wasn't to me until I thought about it), the analog ports
 * measure the voltage between 0v -> Vcc which for an Arduino is a nominal 5v, but for (say)
 * a JeeNode, is a nominal 3.3v.
 *
 * The resistance calculation uses the ratio of the two resistors, so the voltage
 * specified above is really only required for the debugging that is commented out below
 *
 * Resistance = PadResistor * (1024/ADC -1)  
 *
 * I have used this successfully with some CH Pipe Sensors (http://www.atcsemitec.co.uk/pdfdocs/ch.pdf)
 * which be obtained from http://www.rapidonline.co.uk.
 *
 */

// Our NTC: NTCS0603E3103*HT

#include <math.h>

#define ThermistorPIN 0                 // Analog Pin 0

float vcc = 5.01;                       // only used for display purposes, if used
                                        // set to the measured Vcc.
float pad = 9920;                      // balance/pad resistor value, set this to
                                        // the measured resistance of your pad resistor
//float thermr = 10000;                   // thermistor nominal resistance
float padresistors[6] = {1e4,1e5,1e5,1e5,1e5,1e5}; 
float Tinterlock = 30 ; // degrees celsius

float Thermistor(float Resistance) {  

  float logR = log( Resistance ) ;
  
  const float a = 0.001129148 ;
  const float b = 0.000234125 ;
  const float c = 0.0000000876741 ;
  //const float a = 1.4e-3 ;
  //const float b = 2.37e-4 ;
  //const float c = 9.90e-8 ;
  
  // Steinhart-Hart equation (https://en.wikipedia.org/wiki/Thermistor#NTC)
  float Temp = 1 / (a + (b * logR) + (c * logR * logR * logR));
  Temp = Temp - 273.15;  // Convert Kelvin to Celsius                      

  // Uncomment this line for the function to return Fahrenheit instead.
  //temp = (Temp * 9.0)/ 5.0 + 32.0;                  // Convert to Fahrenheit
  return Temp;                                      // Return the Temperature
}

float thermistorVeloNTC( float r ) {
  const float tmin = -40.;
  const float dt   = 5. ; // binsize
  const int N = 23 ;
  const float Rtable[] = { 
    347116.,250089.,182023.,133804.,99313.,74408.,
    56257., 42910.,33009, 25602.,20015.,15767.,12512.,10000.,
    8046.8,6517.6,5312.5,4356.6, 3593.6, 2981.0, 2486.2, 2084.3, 1756.2 } ;  
  if( r>Rtable[0]) return tmin ;
  int i;
  for(i=0; i<N; ++i) {
    if( r>Rtable[i] ) break ;
    /*
    Serial.print(i) ;
    Serial.print(",") ;
    Serial.print(tmin + 5*i) ;
    Serial.print(",") ;
    Serial.print(Rtable[i]) ;
    Serial.println("");
  */
  }
  if( i==N ) return tmin + N*dt ;

  // linear interpolation? the bins are far too large. perhaps we better fit that function.
  const float x = (r - Rtable[i-1]) / (Rtable[i] - Rtable[i-1]) ;
  return tmin + (i-1+x)*dt ; 
}


const int GREENPIN = 2 ;
const int REDPIN   = 3 ;

void setup() {
  Serial.begin(9600);
  pinMode(GREENPIN, OUTPUT) ; 
  pinMode(REDPIN, OUTPUT) ;
  thermistorVeloNTC(0) ;
}

void loop() {

  int adc ;
  float temp ;
  float tempmax = -99999 ;
  float Rntc ; // resistance of the NTC
  int ichan=0;
  for(ichan=0; ichan<6; ++ichan) {
    adc = analogRead(ichan) ;
    Rntc = padresistors[ichan] / (1024.0/adc - 1);
    temp=Thermistor(Rntc);       // read ADC and  convert it to Celsius
    if(tempmax<temp) tempmax = temp;
    Serial.print(ichan) ;
    Serial.print(",");
    Serial.print(adc) ;
    Serial.print(",");
    Serial.print(Rntc,1) ;
    Serial.print(",");
    Serial.print(temp,1) ;
    Serial.print(";");
    Serial.print(thermistorVeloNTC(Rntc)) ;
    Serial.print(";");
    Serial.println("");
  }
  
  digitalWrite(GREENPIN, HIGH ) ;
  if( tempmax<Tinterlock ) {
    digitalWrite(REDPIN,HIGH) ;
  } else {
    digitalWrite(REDPIN,LOW) ;
  } 
 
  delay(2000);                                      // Delay a bit...
 
}
