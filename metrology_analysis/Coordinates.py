import numpy

class Point2D:
    __slots__ = [ "m", "name", "title" ]
    def __init__(self, x=0., y=0., name="", title="" ):
        self.m = numpy.array( [[x],[y]] )
        self.name = name
        self.title = title
    def x(self): return self.m[0,0]
    def y(self): return self.m[1,0]
    def distance(self, rhs ):
        from math import sqrt
        dx = rhs.x() - self.x()
        dy = rhs.y() - self.y()
        return sqrt(dx*dx+dy*dy)
    def __str__(self):
        return "Point2D: %s x=%f, y=%f" %(self.name,self.x(),self.y())

class Transform2D:
    __slots__ = [ "translation","rotation","name" ]
    def __init__(self, translation = numpy.zeros( (2,1) ) ,
                     rotation = numpy.identity( 2 ),
                     name = "Noname",
                     parameters = None ) :
        import copy
        self.translation = copy.deepcopy(numpy.array(translation))
        self.rotation = copy.deepcopy(numpy.array(rotation))
        self.name = name
        if parameters:
            self.setTranslation(parameters[0],parameters[1])
            self.setRotation(parameters[2])
    def setRotation( self, angle ):
        from math import cos,sin
        self.rotation[0,0] = cos(angle)
        self.rotation[0,1] = -1*sin(angle)
        self.rotation[1,0] = -self.rotation[0,1]
        self.rotation[1,1] = self.rotation[0,0]
    def setTranslation( self, dx, dy ):
        self.translation[0,0] = dx
        self.translation[1,0] = dy
    def setParameters( self, pars):
        self.setRotation(pars[2])
        self.setTranslation(pars[0],pars[1])
    def parameters(self) :
        from math import atan2
        return [ self.translation[0,0], self.translation[1,0], -1*atan2(self.rotation[0,1], self.rotation[0,0]) ]
    def __mul__(self,other):
        if( isinstance(other,Point2D) ) :
            rc = Point2D( name = other.name)
            rc.m = self.translation + self.rotation.dot(other.m)
            return rc
        elif( isinstance(other,Transform2D) ) :
            product = Transform2D(name = "%s * %s" % (self.name,other.name))
            product.translation = self.translation + self.rotation.dot(other.translation )
            product.rotation = self.rotation.dot(other.rotation)
            return product
    def inverse(self):
        rc = Transform2D(name = "inverse of %s" % self.name )
        det = self.rotation[0,0] * self.rotation[1,1] - self.rotation[1,0] * self.rotation[0,1]
        rc.rotation[0,0] = self.rotation[1,1]/det
        rc.rotation[1,1] = self.rotation[0,0]/det
        rc.rotation[0,1] = -self.rotation[0,1]/det
        rc.rotation[1,0] = -self.rotation[1,0]/det
        rc.translation = -1 * rc.rotation.dot(self.translation)
        return rc
    def dump(self):
        from math import atan2
        angle = atan2( self.rotation[0,1], self.rotation[0,0])
        print("Transform3D: %s dx=%6.3f dy=%6.3f dphi=%6.5f" % (self.name, self.translation[0], self.translation[1], angle) )
    
    def __str__(self):
        return "Matrix: %s\n translation:\n %s\n rotation:\n %s\n" % (self.name,self.translation,self.rotation)

class ParametrizedTransform2D:
    def __init__(self, parameters = None, covariance = None, name = "" ) :
        self.name = name
        if not parameters is None: self.parameters = parameters
        else: self.parameters = numpy.zeros( (3,1) )
        if not covariance is None: self.covariance = covariance
        else: self.covariance = numpy.zeros( (3,3) )
    def toTransform2D(self):
        rc = Transform2D()
        rc.setParameters( self.parameters )
        return rc
    def fromTransform2D(self, T):
        self.parameters = T.parameters()
    def __mul__(self,other):
        if( isinstance(other,ParametrizedTransform2D) ) :
            # this also propagates errors, but for now it only works for small rotations
            transform = self.toTransform2D() * other.toTransform2D()
            rc = ParametrizedTransform2D()
            rc.fromTransform2D( transform )
            rc.covariance = self.covariance + other.covariance
            return rc
        else:
            return self.toTransform2D()*other
    def inverse(self):
        # this does not really propagate errors. it's probably fine for small rotations.
        rc = ParametrizedTransform2D()
        rc.name = self.name + "Inv"
        rc.covariance  = self.covariance
        rc.fromTransform2D( self.toTransform2D().inverse() )
        return rc
    def correlationMatrix(self):
        from math import sqrt
        rc = numpy.zeros( (3,3) )
        for i in range(0,3):
            for j in range(0,3):
                rc[i,j] = self.covariance[i,j] / sqrt( self.covariance[i,i] * self.covariance[j,j])
        return rc
        
    def __str__(self):
        import math
        return "%s (%f,%f,%f) +/- (%f,%f,%f)" % \
          (self.name,
            self.parameters[0],self.parameters[1],self.parameters[2],
            math.sqrt(self.covariance[0,0]),math.sqrt(self.covariance[1,1]),math.sqrt(self.covariance[2,2]) )
    
class Accumulator :
    def __init__( self, parnames ):
        dim = len(parnames)
        self.parnames = parnames
        self.dim = dim
        self.parameters = numpy.zeros( (dim,1), numpy.float64 )
        self.covariance = numpy.zeros( (self.dim,self.dim), numpy.float64 )
        self.reset()

    def reset(self):
        self.dChi2dPar = numpy.zeros( (self.dim,1), numpy.float64 )
        self.d2Chi2dPar2 = numpy.zeros( (self.dim,self.dim), numpy.float64 )
        self.chi2 = 0
        self.ndof = -self.dim

    def index(self, parname):
        index = self.parnames.index( parname )
        if index<0 : raise LookupError
        return index
    
    def value(self, parname ):
        return self.parameters[ self.index(parname) ]

    def add(self, residual, weight, H ):
        # let's just turn H into a matrix
        matrixH = numpy.zeros( (self.dim, 1) )
        for parname in H.keys():
            index = self.parnames.index( parname )
            if index<0 : raise LookupError
            matrixH[index][0] = H[parname]
        # now add
        self.ndof += 1
        self.dChi2dPar += (0.5*weight*residual) * matrixH
        self.d2Chi2dPar2 += (0.5*weight) * matrixH * matrixH.transpose() 
        self.chi2 += weight*residual*residual
        
class AccumulatorForParametrizedTransform2Ds:
    def __init__( self, transformnames ):
        dim = 3*len(transformnames)
        self.transformnames = transformnames
        self.dim = dim
        self.parameters = numpy.zeros( (dim,1), numpy.float64 )
        self.covariance = numpy.zeros( (self.dim,self.dim), numpy.float64 )
        self.reset()

    def reset(self):
        self.dChi2dPar = numpy.zeros( (self.dim,1), numpy.float64 )
        self.d2Chi2dPar2 = numpy.zeros( (self.dim,self.dim), numpy.float64 )
        self.chi2 = 0
        self.ndof = -self.dim

    def index(self, transformname):
        index = self.transformnames.index( transformname )
        if index<0 : raise LookupError
        return index

    def parameters(self, transformname):
        offset = 3*self.index(transformname)
        return self.parameters[ offset:offset+3,0]

    def transform(self, transformname=None, index=None):
        if not index: index = self.index(transformname)
        offset = 3*index
        thiscurrent = self.parameters[offset:offset+3,0]
        current = Transform2D(name = transformname)
        current.setParameters( thiscurrent )
        return current

    def parametrizedTransform( self, transformname ):
        offset = 3*self.index(transformname)
        return ParametrizedTransform2D( name = transformname,
                                        parameters = self.parameters[offset:offset+3,0],
                                        covariance = self.covariance[offset:offset+3, offset:offset+3] )

    def add(self, residual, weight, H ):
        # let's just turn H into a matrix
        matrixH = numpy.zeros( (self.dim, 1) )
        for name in H.keys():
            index = self.index(name)
            #print("index: ",name, index)
            subH = H[name]
            matrixH[3*index,  0] = subH[0,0]
            matrixH[3*index+1,0] = subH[1,0]
            matrixH[3*index+2,0] = subH[2,0]
        # now add
        self.ndof += 1
        self.dChi2dPar += (0.5*weight*residual) * matrixH
        self.d2Chi2dPar2 += (0.5*weight) * matrixH * matrixH.transpose() 
        self.chi2 += weight*residual*residual

    def update(self):
        import numpy as np
        # compute the delta
        self.covariance = np.linalg.inv( self.d2Chi2dPar2 )
        deltapar = - np.linalg.solve( self.d2Chi2dPar2, self.dChi2dPar )
        self.chi2 -= 0.5* numpy.vdot(deltapar,self.dChi2dPar)
        # now deal with the fact that these are transforms, such that we cannot just 'add' the parameters
        for n in range(0,len(self.transformnames)):
            offset = 3*n
            thisdelta = deltapar[offset:offset+3,0]
            thiscurrent = self.parameters[offset:offset+3,0]
            #print(deltapar)
            #print(parameters)
            #import Coordinates
            delta = Transform2D()
            delta.setParameters( thisdelta )
            current = Transform2D()
            current.setParameters( thiscurrent )
            newtransform = delta * current
            newpars = newtransform.parameters()
            self.parameters[offset,0] = newpars[0]
            self.parameters[offset+1,0] = newpars[1]
            self.parameters[offset+2,0] = newpars[2]


            
