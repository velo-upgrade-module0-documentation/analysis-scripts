import numpy as np
import math
import copy
from Coordinates3D import Point3D as Point3D
from Coordinates3D import Transform3D as Transform3D
from enum import Enum
import TileInfo
from NominalMarkerPositions import ZRESOLUTION

class Plane3D :
  Z       = 0
  DZDX    = 1
  DZDY    = 2
  D2ZDX2  = 3
  D2ZDXDY = 4
  D2ZDY2  = 5
  
  #__slots__ = [ "x", "y", "pars", "tx", "ty", "d2zdx2", "d2zdxy", "d2zdy2", "chi2dof", "name"]
  __slots__ = [ "x", "y", "pars", "cov", "chi2", "ndof", "name" ]
  def __init__(self, name = "", x = 0., y=0.):
    self.name = name
    self.x = x
    self.y = y
    self.pars = np.zeros( (6) )
    self.cov  = np.zeros( (6,6) )
    self.chi2 = 0
    self.ndof = 0
    self.pars[Plane3D.Z]
    # this is essentially just an enum

  def z(self, x, y ):
    dx = x - self.x
    dy = y - self.y
    return self.pars[Plane3D.Z] + dx*self.pars[Plane3D.DZDX] + dy*self.pars[Plane3D.DZDY] + \
           dx*dx*self.pars[Plane3D.D2ZDX2] + dx*dy*self.pars[Plane3D.D2ZDXDY] + dy*dy*self.pars[Plane3D.D2ZDY2]
  def zcov(self, x, y):
    dx = x - self.x
    dy = y - self.y
    H = np.array( [ [1.0], [dx], [dy], [dx*dx], [dy*dx], [dy*dy] ], np.float64 )
    return H.transpose().dot(self.cov).dot(H)

  def derivative( self, x, y ):
      dx = x - self.x
      dy = y - self.y
      dzdx = self.pars[Plane3D.DZDX] + 2*dx*self.pars[Plane3D.D2ZDX2] + dy*self.pars[Plane3D.D2ZDXDY]
      dzdy = self.pars[Plane3D.DZDY] + 2*dy*self.pars[Plane3D.D2ZDY2] + dx*self.pars[Plane3D.D2ZDXDY]
      return np.array( [dzdx,dzdy] )
  def position( self, x, y ):
    return Point3D( x,y,self.z(x,y) )
  def movePivotTo( self, x, y ):
    dx = x - self.x
    dy = y - self.y
    z  = self.z(x,y)
    deriv = self.derivative(x,y)

    # we need a 'transport' matrix for the covariance matrix
    F  = np.identity( n=6 )
    F[Plane3D.DZDX,Plane3D.D2ZDX2]    = 2*dx
    F[Plane3D.DZDX,Plane3D.D2ZDXDY]   = dy
    F[Plane3D.DZDY,Plane3D.D2ZDY2]    = 2*dy
    F[Plane3D.DZDY,Plane3D.D2ZDXDY]   = dx
    covnew   = F.dot(self.cov).dot(F.transpose())
    self.cov = covnew
    self.x = x
    self.y = y
    #print("parameters transported:", F.dot( self.pars) )
    self.pars[Plane3D.Z]    = z
    self.pars[Plane3D.DZDX] = deriv[0] 
    self.pars[Plane3D.DZDY] = deriv[1] 
    #print("parameters:", self.pars)
  
  def createTransform3DOld( self, x, y):
    # this one is wrong because the pivot point for rotation is wrong.
    # this is tricky now. create a transform such that
    # - the point x,y is just moved by te z-position of the surface
    # - any other point is transformed such that its z-position is more or less okay (linear approximation)
    transform = Transform3D(name=self.name)
    transform.setTranslation( dx=0, dy=0, dz=self.z(x,y) )
    d = self.derivative(x,y)
    transform.setRotation( rotZ = 0, rotY = -d[0], rotX = -d[1] )
    print("createTransform3D: NOT TERRIBLY USEFULL YET. Should do this in the right frame?!")
    print(transform)
    return transform

  def createTransform3D( self, x0=None, y0=None):
    if not x0 : x0 = self.x
    if not y0 : y0 = self.y
    # this returns a transformation matrix in the global frame, using the
    # displacement and orientation of the plane at x0,y0.
    
    # this is tricky now. what we would like to have is is a transform that does:
    # x' = x
    # y' = y
    # z' = z + dz0 + (x - x0) * tx0 + (y-y0) * ty0
    # where dz0, tx0 and ty0 are all evaluated from x0,y0. of course, this is
    # not a proper rotation, so at some point we'll need to 'rectify' it.
    # first start with what we believe it should do:
    dz0   = self.z(x0,y0)
    deriv = self.derivative(x0,y0)
    transform = Transform3D(name=self.name)
    transform.translation[2] = dz0 - deriv[0]*x0 - deriv[1]*y0
    transform.rotation[2,0] = deriv[0]
    transform.rotation[2,1] = deriv[1]
    
    #print("createTransform3D: in the global frame")
    #print(transform)
    # now try to obtain the same result using proper rotations:
    transform.setRotation( rotZ = 0, rotY = -deriv[0], rotX = deriv[1] )
    # I checked that this reproduces the right derivatives
    
    #transform.rotation[0,2] = -transform.rotation[2,0]
    #transform.rotation[2,1] = -transform.rotation[2,1]
    #transform.rotation[2,2] = math.sqrt( 1 - transform.rotation[2,0]**2 - transform.rotation[2,1]**2)
    #rot = transform.rotationParameters()
    #transform.setRotation( rot[0], rot[1], rot[2] )
    #print("after rectifying")
    #print(transform)

    return transform

  
  def __str__(self):
    return "Plane3D "

def residual( point, plane ):
  return point.z() - plane.z( point.x(), point.y() )

def residualUnbiasedRMS( point, plane ):
  r   = point.z() - plane.z( point.x(), point.y() )
  V   = ZRESOLUTION**2
  HCH = plane.zcov( point.x(), point.y() )
  R   = V - HCH
  from math import sqrt
  return r*sqrt(V/R)

def fitPlane( points, curved, verbose=False, pivot=None ):
  if pivot:
    x0 = pivot.x()
    y0 = pivot.y()
    z0 = pivot.z()
  else:
    cog = np.zeros( (3,1) )
    for p in points: cog += p.m
    cog /= len(points)
    x0 = cog[0,0]
    y0 = cog[1,0]
    z0 = cog[2,0]
  
  d2Chi2dX2  = np.zeros( (6,6) )
  dChi2dX    = np.zeros( (6) )
  sigmaz = ZRESOLUTION
  weight = 1/sigmaz**2
  for p in points  :
    # the model is
    #   z = par(0) + par(1) * (x - x0) + par(2) * ( y - y0 ) + par(3) *  (x - x0) *  (x - x0) + ...
    # residual = z(model) - z
    res = z0-p.z()
    dx = p.x()-x0
    dy = p.y()-y0
    H = np.array( [ [1.0], [dx], [dy], [dx*dx], [dy*dx], [dy*dy] ], np.float64 )
    dChi2dX   += 2 * weight * H[0:6,0] * res
    d2Chi2dX2 += 2 * weight * H.dot(H.transpose())
    
  # compute the parameters. if it is not a curved plane, we need to reduce the dimensions
  np.set_printoptions(linewidth=120)
  if curved:
    cov  = np.linalg.inv(d2Chi2dX2)
    pars = - cov.dot(dChi2dX)
  else:
    dChi2dXsub   = dChi2dX[0:3]
    d2Chi2dX2sub = d2Chi2dX2[0:3,0:3]
    covsub       = np.linalg.inv(d2Chi2dX2sub)
    parssub      = - covsub.dot(dChi2dXsub)
    pars = np.zeros( (6) )
    pars[0:3] = parssub
    cov  = np.zeros( (6,6) )
    cov[0:3,0:3] = covsub

  #print("cov matrix of plane fit: ", cov)
  pars[0] += z0
  fittedplane = Plane3D()
  fittedplane.x = x0
  fittedplane.y = y0
  fittedplane.pars = pars
  fittedplane.cov  = cov
  
  #print(fittedplane.p, fittedplane.tx, fittedplane.ty, fittedplane.d2zdx2, fittedplane.d2zdxy, fittedplane.d2zdy2) 
  #print("z-plane:", fittedplane.p.z() )
  # perhaps compute the chi2 (in units of distance), the lazy way:
  chi2 = 0.
  for p in points:
      r = residual( point=p,plane=fittedplane)
      chi2 += r*r*weight
      if verbose: print("residual: %s %f" % (p.name,r))
  ndof = len(points)-6
  fittedplane.chi2 = chi2
  fittedplane.ndof = ndof
  if verbose:
    print("chi2 of fitted plane: %f/%d" % (chi2,ndof) )
    
  return fittedplane

def fitFlatPlane( points, verbose=False, pivot=None ):
    return fitPlane( points = points, curved=False, verbose = verbose, pivot=pivot )

def fitCurvedPlane( points, verbose=False, pivot=None ):
    return fitPlane( points = points, curved=True, verbose = verbose , pivot=pivot)

def drawZResiduals( points, plane, modulename=None ):
     # plot a histogram of the residuals
    residuals = [ residual(p,plane) for p in points ]    
    import matplotlib.pyplot as plt
    fig, ax1 = plt.subplots(1,1)
    ax1.set_xlabel('Z residual [mm]')
    plt.rcParams.update({'figure.figsize':(7,5), 'figure.dpi':100})
    rms = np.sqrt(np.mean(np.square(residuals)))
    print("residual rms: ",rms)
    ax1.hist(residuals, bins=40, range=(-0.02,0.02) )
    ax1.text(0.01,5.5,"rms: %6.4f" % rms)
    if modulename: ax1.set_title(modulename)
    if modulename: fig.savefig('zresiduals_%s.pdf'%modulename)
    else:          fig.savefig('zresiduals.pdf')

def drawTileZResiduals( moduledata ):
    import matplotlib.pyplot as plt
    fig, ax1 = plt.subplots(1,1)
    ax1.set_xlabel('Z residual [mm]')
    plt.rcParams.update({'figure.figsize':(7,5), 'figure.dpi':100})
    residualarrays = []
    labels = []
    for t in moduledata.tiledata:
        labels.append(t)
        points = moduledata.tiledata[t].sensorpoints
        plane = fitCurvedPlane( points )
        residuals = [ residualUnbiasedRMS(p,plane) for p in points ]
        residualarrays.append( residuals )
    ax1.hist(residualarrays, bins=40, range=(-0.02,0.02), stacked=True, label=labels )
    ax1.legend()
    rms = np.sqrt(np.mean(np.square(residuals)))
    ax1.text(0.01,5.5,"rms: %6.4f" % (rms))
    ax1.set_title(moduledata.name)
    fig.savefig('tilezresiduals_%s.pdf'%moduledata.name)

def correlationmatrix( V ):
    from math import sqrt
    C = copy.deepcopy(V)
    N = C.shape[0]
    for i in range(0,N):
        for j in range(0,N):
            C[i,j] = V[i,j]/sqrt(V[i,i]*V[j,j])
    return C

def addTileZAlignmentTransforms( moduledata, constrainCommonCurvature = False, verbose=False ):
    # fit planes where we need them
    #from FitPlane import fitCurvedPlane, fitFlatPlane, Plane3D
    centre = TileInfo.sensorCentreOfGravity()
    moduledata.csidesubstratefit = fitCurvedPlane(moduledata.csidesubstratepoints,pivot=centre)
    moduledata.nsidesubstratefit = fitCurvedPlane(moduledata.nsidesubstratepoints,pivot=centre)
    for t in moduledata.tiledata.values() :
        t.sensorsurfacefit = fitFlatPlane(t.sensorpoints,pivot=centre)

    # extract transforms to get the z measurements in a common frame
    #centre = TileInfo.sensorCentreOfGravity()
    #moduledata.csidesubstratefit.movePivotTo( centre.x(), centre.y() )
    #moduledata.nsidesubstratefit.movePivotTo( centre.x(), centre.y() )
    
    #altfit = fitCurvedPlane(moduledata.nsidesubstratepoints, pivot = centre)
    #print("substrate fit parameters N-side: ", altfit.pars )
    #print(altfit.z( centre.x(), centre.y() ), moduledata.nsidesubstratefit.z( centre.x(), centre.y() ))
    #print(altfit.derivative( centre.x(), centre.y() ), moduledata.nsidesubstratefit.derivative( centre.x(), centre.y() ))
    if verbose: print("substrate fit parameters C-side: ", moduledata.csidesubstratefit.pars )
        
    if constrainCommonCurvature:
        # create z new parameter set such that local curvatures becomes 'combined'
        # (z_n, tx_n, ty_n, z_c, tx_c, ty_c, d2zdx2, d2zdxy, d2zdy2)
        import numpy as np
        Hn  = np.zeros( (6,9) )
        Hn[0,0] = Hn[1,1] = Hn[2,2] = Hn[3,6] = Hn[4,7] = Hn[5,8] = 1.0
        Hc  = np.zeros( (6,9) )
        Hc[0,3] = Hc[1,4] = Hc[2,5] = Hc[3,6] = Hc[4,7] = Hc[5,8] = 1.0
        Wn = np.linalg.inv(moduledata.nsidesubstratefit.cov)
        Wc = np.linalg.inv(moduledata.csidesubstratefit.cov)
        Wx = Hn.transpose().dot(Wn).dot(moduledata.nsidesubstratefit.pars) + Hc.transpose().dot(Wc).dot(moduledata.csidesubstratefit.pars)
        W  = Hn.transpose().dot(Wn).dot(Hn) + Hc.transpose().dot(Wc).dot(Hc)
        newpars = np.linalg.inv(W).dot(Wx)
        if verbose:
            print("weighted average: ", newpars )
            print("n: ",  moduledata.nsidesubstratefit.pars[3:6])
            print("c: ",  moduledata.csidesubstratefit.pars[3:6])
            print("nc:",  newpars[6:9])    
            print("SOMETHING IS WRONG: weighted average is not in between")
            cov9x9 = np.linalg.inv(W)
            print("Vc", moduledata.csidesubstratefit.cov[3:6,3:6])
            print("Vn", moduledata.nsidesubstratefit.cov[3:6,3:6])
            print("V", cov9x9[6:9,6:9] )
            print("Cc", correlationmatrix(moduledata.csidesubstratefit.cov[3:6,3:6]))
            print("Cn", correlationmatrix(moduledata.nsidesubstratefit.cov[3:6,3:6]))
            print("C", correlationmatrix(cov9x9[6:9,6:9]))
        moduledata.nsidesubstratefit.pars[0:3] = newpars[0:3]
        moduledata.nsidesubstratefit.pars[3:6] = newpars[6:9]
        moduledata.csidesubstratefit.pars[0:3] = newpars[3:6]
        moduledata.csidesubstratefit.pars[3:6] = newpars[6:9]
        
    # the next step is to create from this transforms by which we can multiply
    # the sensor transforms on both sides such that they are in a common
    # frame. need to think ab bit about how we could do this. in the end we
    # need the transforms in one frame. something like
    #    sensordelta = planefit.inverse() * sensorplane
    # but not quite :-)    
    #clisensorsurfacetransform = moduledata.tiledata["CLI"].sensorsurfacefit.createTransform3D()
    #deltaclisensorsurfacetransform = moduledata.csidesubstratefitconstrained.createTransform3D(centre.x(),centre.y()).inverse() * clisensorsurfacetransform
    #if verbose:
    #    print("original c-side sensor:")
    #    print(clisensorsurfacetransform)
    #    print("after correcting for displacement of substrate:")
    #    print(deltaclisensorsurfacetransform)
    #    print("This is not yet right, because we really want the transform relative to the 'nominal' z position. Perhaps it is easies just to subtract something?"\
    #          "Or better express everything in terms of delta-z and slopes and create the transform afterwards?")

    for (tiles, substratefit, zcorrection) in \
      zip( [ ["CLI","CSO"],["NSI","NLO"] ], [moduledata.csidesubstratefit,moduledata.nsidesubstratefit], [+0.49,-0.49] ) :
        for tile in tiles:
            tiledata = moduledata.tiledata[tile]
            deltasensorsurfacefit = copy.deepcopy(tiledata.sensorsurfacefit)
            #deltaclisensorsurfacefit.movePivotTo( centre.x(), centre.y() )
            deltasensorsurfacefit.pars[0:3] -= substratefit.pars[0:3]
            deltasensorsurfacefit.pars[0]   += zcorrection # nominal distance between sensor surface and substrate surface
            deltasensorsurfacetransform =  deltasensorsurfacefit.createTransform3D()
            deltasensorsurfacetransform.name = tiledata.name
            #print(deltasensorsurfacetransform )
            tileinfo = TileInfo.tileinfodict[tiledata.name]
            nominaltransform = tileinfo.NominalTransform
            #print("deltaz before:",deltasensorsurfacetransform)            
            tiledata.DeltaZTransform = nominaltransform.inverse() * deltasensorsurfacetransform * nominaltransform
            tiledata.DeltaZTransform.name = "%s DeltaZ local" % tiledata.name
            #print("deltaz after:",tiledata.DeltaZTransform)
            if verbose: print("Tile, Deltaz: ", tile, tiledata.DeltaZTransform.translation[2])

    # let's check if it is 'leaning' in the right direction
    #for p in cliinfo.corners():
    #    print(deltaclisensorsurfacetransform*cliinfo.transform()*p)
    #    print(cliinfo.transform()*cliinfo.DeltaZTransform*p)
    #print( cliinfo.DeltaZTransform )



