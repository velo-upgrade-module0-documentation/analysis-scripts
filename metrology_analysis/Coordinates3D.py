import numpy

TX = 0
TY = 1
TZ = 2
RX = 3
RY = 4
RZ = 5

class Point3D:
    __slots__ = [ "m", "name", "title" ]
    def __init__(self, x=0., y=0., z=0., name="", title=None):
        self.m = numpy.array( [ [x],[y],[z] ] )
        self.name = name
        self.title = title
        if not self.title: self.title = self.name
    def x(self): return self.m[0,0]
    def y(self): return self.m[1,0]
    def z(self): return self.m[2,0]
    def distance(self, rhs ):
        from math import sqrt
        dx = rhs.x() - self.x()
        dy = rhs.y() - self.y()
        dz = rhs.z() - self.z()
        return sqrt(dx*dx+dy*dy+dz*dz)
    def __str__(self):
        return "Point3D: %s %s x=%6.4f, y=%6.4f, z=%6.4f" %(self.name,self.title,self.x(),self.y(),self.z())

class Transform3D:
    __slots__ = [ "translation","rotation","name" ]
    def __init__(self, translation = numpy.zeros( (3,1) ) ,
                     rotation = numpy.identity( 3 ),
                     name = "Noname",
                     parameters = None ) :
        import copy
        self.translation = copy.deepcopy(numpy.array(translation))
        self.rotation = copy.deepcopy(numpy.array(rotation))
        self.name = name
        #if parameters:
        #    self.setTranslation(parameters[0],parameters[1],parameters[2])
        #    self.setRotation(parameters[3])
    def setRotation( self, rotX=0, rotY=0, rotZ=0 ):
        from scipy.spatial.transform import Rotation as R
        r = R.from_euler('zyx',[ rotZ, rotY, rotX ] )
        self.rotation = r.as_matrix()
    def setTranslation( self, dx=0, dy=0, dz=0 ):
        self.translation[0,0] = dx
        self.translation[1,0] = dy
        self.translation[2,0] = dz
    def rotationParameters(self) :
        from scipy.spatial.transform import Rotation as R
        r = R.from_matrix( self.rotation)
        decomposition = r.as_euler('zyx')
        return [ decomposition[2], decomposition[1], decomposition[0] ]
    def parameters(self):
        rot    = self.rotationParameters()
        trans  = self.translation
        return [ trans[0], trans[1], trans[2], rot[0], rot[1], rot[2] ]
    def setParameters(self, dx, dy, dz, rotX, rotY, rotZ ):
        self.setRotation( rotX=rotX, rotY=rotY, rotZ=rotZ )
        self.setTranslation( dx=dx, dy=dy, dz=dz)
        
    def __mul__(self,other):
        if( isinstance(other,Point3D) ) :
            rc = Point3D(name=other.name,title=other.title)
            rc.m = self.translation + self.rotation.dot(other.m)
            return rc
        elif( isinstance(other,Transform3D) ) :
            product = Transform3D(name = "%s * %s" % (self.name,other.name))
            product.translation = self.translation + self.rotation.dot(other.translation )
            product.rotation = self.rotation.dot(other.rotation)
            return product
        return NotImplemented

    def inverse(self):
        rc = Transform3D(name = "inverse of %s" % self.name )
        from numpy.linalg import inv as invert
        rc.rotation = invert( self.rotation )
        rc.translation = -1 * rc.rotation.dot(self.translation)
        return rc
    def dump(self):
        print(self)        
    def __str__(self):
        rot = self.rotationParameters()
        return "Transform3D: %s dx=%6.3f dy=%6.3f dz=%6.3f rotX=%6.5f rotY=%6.3f rotZ=%6.5f" % \
                  (self.name, self.translation[0], self.translation[1], self.translation[2],
                       rot[0], rot[1], rot[2])
        return "Matrix: %s\n translation:\n %s\n rotation:\n %s\n" % (self.name,self.translation,self.rotation)

def rotationZ( theta ):
    from scipy.spatial.transform import Rotation as R
    return Transform3D(rotation = R.from_euler('z',theta).as_matrix())

def rotationY( theta ):
    from scipy.spatial.transform import Rotation as R
    return Transform3D(rotation = R.from_euler('y',theta).as_matrix())

def rotationX( theta ):
    from scipy.spatial.transform import Rotation as R
    return Transform3D(rotation = R.from_euler('x',theta).as_matrix())

def translation3D( Tx, Ty, Tz ):
    rc = Transform3D()
    rc.setTranslation( Tx, Ty, Tz)
    return rc

class ParametrizedTransform3D:
    def __init__(self, parameters = None, covariance = None, name = "" ) :
        self.name = name
        if not parameters is None: self._parameters = parameters
        else: self._parameters = numpy.zeros( (6) )
        if not covariance is None: self._covariance = covariance
        else: self._covariance = numpy.zeros( (6,6) )
    def toTransform3D(self):
        rc = Transform3D()
        rc.setParameters( dx = self._parameters[TX], dy = self._parameters[TY], dz = self._parameters[TZ],
                          rotX = self._parameters[RX], rotY = self._parameters[RY], rotZ = self._parameters[RZ] )
        return rc
    def parameters(self) : return self._parameters
    def uparameters(self):
        from math import sqrt
        from uncertainties import ufloat
        return [ ufloat(self._parameters[i],sqrt(self._covariance[i,i])) for i in range(6) ]

    def covariance(self) : return self._covariance
    def fromTransform3D(self, T):
        if( isinstance(T,ParametrizedTransform3D) ) :
            self._parameters = T._parameters
        elif( isinstance(T,Transform3D) ) :
            self._parameters = T.parameters()
        return NotImplemented
    def __mul__(self,other):
        #this also propagates errors, but for now it only works for small rotations
        if( isinstance(other,ParametrizedTransform3D) ) :
            transform = self.toTransform3D() * other.toTransform3D()
            rc = ParametrizedTransform3D()
            rc.fromTransform3D( transform )
            rc._covariance = self._covariance + other._covariance
            return rc
        elif( isinstance(other,Transform3D) ) :
            transform = self.toTransform3D() * other
            rc = ParametrizedTransform3D()
            rc.fromTransform3D( transform )
            rc._covariance = self._covariance
            return rc
        else:
            return self.toTransform3D()*other
    def __rmul__(self,other):
        #print('rmul', type(self), type(other))
        # this also propagates errors, but for now it only works for small rotations
        if( isinstance(other,ParametrizedTransform3D) ) :
            transform = other.toTransform3D() * self.toTransform3D()
            rc = ParametrizedTransform3D()
            rc.fromTransform3D( transform )
            rc._covariance = self._covariance + other._covariance
            return rc
        elif( isinstance(other,Transform3D) ) :
            transform = other * self.toTransform3D()
            rc = ParametrizedTransform3D()
            rc.fromTransform3D( transform )
            rc._covariance = self._covariance
            return rc
        return NotImplemented
    def inverse(self):
        # this does not really propagate errors. it's probably fine for small rotations.
        rc = ParametrizedTransform3D()
        rc.name = self.name + "Inv"
        rc._covariance  = self._covariance
        rc.fromTransform3D( self.toTransform3D().inverse() )
        return rc
    def __str__(self):
        import math
        return "ParametrizedTransform3D %s (%f,%f,%f,%f,%f,%f) +/- (%f,%f,%f,%f,%f,%f)" % \
          (self.name,
            self._parameters[0],self._parameters[1],self._parameters[2],
            self._parameters[3],self._parameters[4],self._parameters[5],
            math.sqrt(self._covariance[0,0]),math.sqrt(self._covariance[1,1]),math.sqrt(self._covariance[2,2]),
            math.sqrt(self._covariance[3,3]),math.sqrt(self._covariance[4,4]),math.sqrt(self._covariance[5,5])
          )

    
if __name__ == "__main__":
    print("Test 3D transformations")
    import math
    x = Transform3D()
    x.setRotation(math.pi/2,0,0) # note: this is a rotation around the Z-axis: parameters are rotZ, rotY, rotX
    x.dump()
    x.setRotation(0,math.pi/2,0)
    x.dump()
    x.setRotation(0,0,math.pi/2)
    x.dump()
    x = rotationX(math.pi/2)
    x.dump()
    x = rotationY(math.pi/2)
    x.dump()
    x = rotationZ(math.pi/2)
    x.dump()
    x = rotationX(0.2) * rotationY(0.3) * rotationZ(0.4)
    x.dump()

