
# these are in the LHCb frame. It wouldn't hurt to add all the ones
# that are still missing.
TileMarkersInDrawings = {
    "CLI_VP00_Fid1" : (-11.0695,41.4332),
    "CLI_VP00_Fid2" : (-1.1523,31.5161),
    "CLI_VP02_Fid2" : (18.9932,11.3706),
    "CSO_VP32_Fid2" : (21.2100,-28.5992),
    "CSO_VP30_Fid1" : (51.2726,  1.4634),
    "NSI_VP20_Fid1" : (11.4873,-18.8765),
    "NSI_VP20_Fid2" : (21.4044,-8.9594),
    "NSI_VP22_Fid2" : (41.5499, 11.1861),
    "NLO_VP10_Fid1" : (27.7436, 22.0656),
    "NLO_VP12_Fid2" : (-2.3190, 52.1282),
    "CLI_Sensor_Fid1": (-22.7936,29.4793),
    "CLI_Sensor_Fid2":(-22.7936,30.2571),
    "CLI_Sensor_T40":(+5.6374,1.0483),
    "NSI_Sensor_Fid1":(-0.2369,-6.9226),
    "NSI_Sensor_Fid2":(-0.2369,-7.7004),
    "CSO_Sensor_Fid3":(19.1732,-27.1105),
    "CSO_Sensor_L230":(10.3821,-18.3194),
    "NSI_Sensor_T610":(5.7930,-0.8927),
    "NSI_Sensor_R30":(8.4376,-16.3748)
}

# these are in an unknown frame of reference :-)
# unfortunately, I have turned some markers around in the P&P software
SubstrateMarkersInSubstrateFrameCSide = {
    "MC_C00" : (114.5,51.42),
    "MC_C01" : (114.5,10),
    "MC_C02" : (2.0,33.5),
    "MC_C03" : (2.0,77.01),
    "MC_C04" : (28.13,101.41),
    "MC_C05" : (34.36,109.95),
    "MC_C06" : (67.56,76.75),
    "MC_C07" : (58.66,70.88),
    "MC_C08" : (55.91,48.91),
    "MC_C09" : (68.43,38.21),
    "MC_C10" : (85.34,81.17),
    "MC_C11" : (98.96,68.74),
    "MC_C_B1" : (55.95,1.9),  # actually B1
    "MC_C_B2" : (56.0,10.05), # actually B2
    "MC_C_B3" : (84.05,10.1), # actually B3
    "MC_C_B4" : (84.05,1.9)   # actually B4
    }

SubstrateMarkersInSubstrateFrameNSide = {
    "MC_N12" : (114.5,51.42),
    "MC_N13" : (114.5,10.0),
    "MC_N14" : (2.0,33.5),
    "MC_N15" : (2.0,77.01),
    "MC_N16" : (17.43,92.35),
    "MC_N17" : (30.34,105.31),
    "MC_N18" : (60.3,75.36),
    "MC_N19" : (47.96,61.82),
    "MC_N20" : (58.55,48.09),
    "MC_N21" : (89.08,78.62),
    "MC_N_B1" : (55.95,1.9),
    "MC_N_B2" : (55.95,10.1),
    "MC_N_B3" : (84.05,10.1),
    "MC_N_B4" : (84.05,1.9)
    }

SubstrateMarkersInSubstrateFrame = {**SubstrateMarkersInSubstrateFrameCSide, **SubstrateMarkersInSubstrateFrameNSide}

# translate into LHCb frame coordinates. this seems to be right within 100 micron
SubstrateMarkers = {}
for key in SubstrateMarkersInSubstrateFrame:
    nominalposition = SubstrateMarkersInSubstrateFrame[key]
    xnominal = - nominalposition[1] + 90
    ynominal = - nominalposition[0] + 70
    SubstrateMarkers[key] = (xnominal,ynominal)

ZRESOLUTION  = 0.003
XYRESOLUTION = 0.003

if __name__ == "__main__":
    for i in SubstrateMarkers:
        print("{\"%s\", %7.2f, %7.2f}," %(i,SubstrateMarkers[i][0], SubstrateMarkers[i][1] ) )
