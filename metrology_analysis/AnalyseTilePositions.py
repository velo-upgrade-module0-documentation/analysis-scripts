# This scripts makes delta-x/delta-y plots for the markers for one file or one module
# function that draws the deltas. this could go to another module at some point

from Coordinates import AccumulatorForParametrizedTransform2Ds, Transform2D, Point2D
from NominalMarkerPositions import XYRESOLUTION
import TileInfo
import numpy as np

def drawdeltas( measurements, figname="deltas" ) :
    # sort the measurements by tile
    import matplotlib.pyplot as plt
    from TileInfo import TileMarkers as nominalmarkerpositions
    fig, axes = plt.subplots(2,2)
    fig.set_size_inches(10,8)
    fig.subplots_adjust(left=0.1,right=0.95,bottom=0.05,top=0.95,wspace = 0.3,hspace=0.3)
    xmax=0.05
    for point in measurements:
        if point.name in nominalmarkerpositions.keys():
            #point = measurements[name]
            pos = nominalmarkerpositions[point.name]
            dx = point.x()-pos.x()
            dy = point.y()-pos.y()
            marker  = "s"
            if "Sensor" in point.title: marker = "P"            
            if "zeiss" in point.title: marker = "o"
            ix=0
            iy=0
            if "C" == point.name[0]: iy=1
            if "L" == point.name[1]: ix=1
            axes[ix,iy].scatter(dx, dy, label=point.title, marker = marker)
            if xmax<abs(dx): xmax=abs(dx)
            if xmax<abs(dy): xmax=abs(dy)
    for ay in axes:
        for ax in ay:
            ax.set_xlim([-xmax,xmax])
            ax.set_ylim([-xmax,xmax])
            ax.legend(loc="upper right", fontsize="small")
            ax.grid(True)
            ax.set_xlabel('Delta x [mm]')
            ax.set_ylabel('Delta y [mm]')
    fig.savefig('plots/' + figname + '.pdf')
    
def drawdeltassingle( measurements, figname="deltassingle" ) :
    # sort the measurements by tile
    import matplotlib.pyplot as plt
    from TileInfo import TileMarkers as nominalmarkerpositions
    fig, axis = plt.subplots(1,1)
    fig.set_size_inches(10,8)
    fig.subplots_adjust(left=0.1,right=0.95,bottom=0.05,top=0.95,wspace = 0.3,hspace=0.3)
    xmax=0.05
    for point in measurements:
        if point.name in nominalmarkerpositions.keys():
            #point = measurements[name]
            pos = nominalmarkerpositions[point.name]
            dx = point.x()-pos.x()
            dy = point.y()-pos.y()
            marker  = "s"
            if "Sensor" in point.title: marker = "P"            
            if "zeiss" in point.title: marker = "o"
            axis.scatter(dx, dy, label=point.title, marker = marker)
            if xmax<abs(dx): xmax=abs(dx)
            if xmax<abs(dy): xmax=abs(dy)
    axis.set_xlim([-xmax,xmax])
    axis.set_ylim([-xmax,xmax])
    axis.legend(loc="upper right", fontsize="small")
    axis.grid(True)
    axis.set_xlabel('Delta x [mm]')
    axis.set_ylabel('Delta y [mm]')
    fig.savefig('plots/' + figname + '.pdf')

def sortingname( name ):
    if "Sensor" in name: return "Z" + name
    return name
    
def drawpairs( pairs, figname="deltas" ) : # a pair is combination of original measurement and the nominal
    # sort the measurements by tile
    import matplotlib.pyplot as plt
    fig, axes = plt.subplots(2,2)
    fig.set_size_inches(10,8)
    fig.subplots_adjust(left=0.1,right=0.95,bottom=0.05,top=0.95,wspace = 0.3,hspace=0.3)

    fig2, axis2 = plt.subplots(1,2)
    
    for pair in sorted(pairs, key=lambda r: sortingname(r[0].name)) :
        measured = pair[0]
        nominal  = pair[1]
        name     = measured.name
        dx = measured.x()-nominal.x()
        dy = measured.y()-nominal.y()
        marker  = "s"
        if "Sensor" in measured.title: marker = "P"    
        if "zeiss" in measured.title: marker = "o"
        ix=0
        iy=0
        if "C" == measured.name[0]: iy=1
        if "L" == measured.name[1]: ix=1
        axes[ix,iy].errorbar(x=dx, y=dy, yerr=0.003, xerr=0.003, label=measured.title, marker = marker)
        #axes[ix,iy].scatter(dx, dy, label=measured.title, marker = marker)
        axis2[0].scatter(measured.z(),dx)
        axis2[1].scatter(measured.z(),dy)
    for ay in axes:
        for ax in ay:
            ax.set_xlim([-0.02,0.02])
            ax.set_ylim([-0.02,0.02])
            ax.legend(loc="upper right", fontsize="xx-small")
            ax.grid(True)
            ax.set_xlabel('Delta x [mm]')
            ax.set_ylabel('Delta y [mm]')
    fig.savefig('plots/' + figname + '.pdf')
    for a in axis2:
        ax.set_ylim([-0.02,0.02])


def toTransform3D( t2d ):
    from Coordinates3D import Transform3D
    t3d = Transform3D( name = t2d.name )
    t3d.translation[0:2] = t2d.translation
    t3d.rotation[0:2,0:2] = t2d.rotation
    return t3d

def toParametrizedTransform3D( t2d ):
    from Coordinates3D import ParametrizedTransform3D
    t3d = ParametrizedTransform3D( name = t2d.name )
    t3d._parameters[0] = t2d.parameters[0]
    t3d._parameters[1] = t2d.parameters[1]
    t3d._parameters[5] = t2d.parameters[2] #FIXME
    #print("2D transform: ", t2d.toTransform2D().rotation )
    #print("3D transform: ", t3d.toTransform3D().rotation )
    jacobian = np.zeros( (6,3) )
    jacobian[0,0] = jacobian[1,1] = jacobian[5,2 ] = 1
    t3d._covariance = jacobian.dot( t2d.covariance ).dot(jacobian.transpose())        
    return t3d


def fittiletransforms( measurements,
                       floatCToN = True, 
                       CToNExternal = Transform2D(name = "CToNExternal"),
                       verbose = False,
                       ignoreSensorMeasurements = True,
                       modulename = "AnonymousModule"):
    # the parameters are
    # * a 2D transform (dx,dy,dphi) for each of the tiles, just in the global frame
    # * a 2D transform for the c-side relative to the n-side
    tilenames = ["CLI","CSO","NSI","NLO"]
    import copy
    transformnames = copy.deepcopy(tilenames)
    deltaCToN = None
    if floatCToN:
        deltaCToN = "deltaCToN"
        transformnames.append( deltaCToN )

    if verbose: print("floatCToN: ", floatCToN, CToNExternal )
        
    import numpy
    from Coordinates import AccumulatorForParametrizedTransform2Ds, Transform2D, Point2D
    accumulator = AccumulatorForParametrizedTransform2Ds(transformnames)
    transforms = { name : Transform2D(name = name) for name in transformnames }
    pairs = []
    
    from TileInfo import TileMarkers as nominalmarkerpositions
    maxiter = 3
    for iter in range(0,maxiter) :
        accumulator.reset()
        CToN = CToNExternal
        if floatCToN: CToN = CToNExternal * transforms[deltaCToN]
        
        for point in measurements:
            if point.name in nominalmarkerpositions.keys():
                ipos = nominalmarkerpositions[point.name]
                nominalpos = Point2D( x=ipos.x(), y=ipos.y() )
                if "Sensor" in point.name and ignoreSensorMeasurements: continue
                #if verbose: print(point)

                # get the transformname for this marker
                transformname = None
                for n in transformnames:
                    if n in point.name:
                        transformname = n
                if verbose: print(point.name,transformname)
                addCToN = ( ("C" in point.name and not "Sensor" in point.name) or ("N" in point.name and "Sensor" in point.name) )
                delta = transforms[transformname]
                if addCToN: delta = CToN * transforms[transformname]
                            
                pnominal_aligned = delta*nominalpos
                if iter==maxiter-1:
                    pairs.append( (point,pnominal_aligned) )
                if verbose :
                    print( "nominal, aligned, measured: ", point.name, nominalpos, pnominal_aligned, point )
                # sigma^2 and any evt additional weight
                sigma2 = XYRESOLUTION**2
                weight = 1.0
                # add residual in x
                dx = point.x() - pnominal_aligned.x()
                HX = numpy.zeros( (3,1), numpy.float64 )
                HX[0,0] = -1
                HX[1,0] =  0
                HX[2,0] = + pnominal_aligned.y()
                HXdict = { transformname : HX }
                if addCToN and deltaCToN: HXdict[deltaCToN] = HX
                accumulator.add( residual=dx, weight=weight/sigma2, H=HXdict )

                # add residual in y
                dy = point.y() - pnominal_aligned.y()
                HY = numpy.zeros( (3,1), numpy.float64 )
                HY[0,0] =  0
                HY[1,0] = -1
                HY[2,0] = - pnominal_aligned.x()
                HYdict = { transformname : HY }
                if addCToN and deltaCToN: HYdict[deltaCToN] = HY
                accumulator.add( residual=dy, weight=weight/sigma2, H=HYdict )
                # now solve
            else:
                print("Cannot find: ", point.name)
        import numpy
        numpy.set_printoptions(precision=3,linewidth=200)
        if verbose: print(accumulator.dChi2dPar)
        if verbose: print(accumulator.d2Chi2dPar2)

        # call the update and cache the transforms
        accumulator.update()
        for n in transformnames: transforms[n] = accumulator.transform(n)
        
        #chi2 = accumulator.chi2 - 0.5* numpy.vdot(deltapar,accumulator.dChi2dPar)
        #if verbose: print("delta: ", deltapar)
        if verbose: print("chi2, ndof: ", accumulator.chi2, accumulator.ndof)
    # dump the results
    if verbose:
        for n in transformnames:
            print(accumulator.parametrizedTransform(n))
    # shall we plot the residuals in 2D?
    drawpairs(pairs, figname = "tilexyresiduals_%s" % modulename )

    # return a map with the results. also translate into 3D transforms here.
    return { t : toParametrizedTransform3D(accumulator.parametrizedTransform(t)) for t in tilenames }

def addTileXYTransforms( moduledata, useSubstrateForCToN = False, floatCToN = False, verbose = False ):

    CToNExternal = Transform2D(name = "CToNExternal")
    if useSubstrateForCToN:
        CToNExternal = moduledata.SubstrateCToN 
        #if moduledata.csidesubstratepoints and moduledata.nsidesubstratepoints:
        #    substratepoints = moduledata.csidesubstratepoints + moduledata.nsidesubstratepoints
        #    from AnalyseSubstrateMarkerPositions import fitSubstrateTransforms
        #    CToNExternal = fitSubstrateTransforms( substratepoints, moduledata.name, verbose=verbose)
                
    #execute the function (we'll move it to a different module later). we do all of them together
    tilemeasurements = []
    for t in moduledata.tiledata.values() : tilemeasurements += t.markerpoints
    
    result = fittiletransforms( tilemeasurements, floatCToN = floatCToN, CToNExternal = CToNExternal, verbose=verbose,
                                    modulename = moduledata.name)
    for tiledata in moduledata.tiledata.values() :
        deltaglobal = result[tiledata.name]
        tileinfo = TileInfo.tileinfodict[tiledata.name]
        nominaltransform = tileinfo.NominalTransform
        tiledata.DeltaXYTransform = nominaltransform.inverse() * deltaglobal * nominaltransform
        tiledata.DeltaXYTransform.name = "%s local delta XY" % tiledata.name

        if verbose:
            print( tiledata.name, "global delta: " , deltaglobal, "local delta: ", tiledata.DeltaXYTransform)
        
# the main function
def main():
    from optparse import OptionParser
    parser = OptionParser(usage="%prog [options] <directory or module name> ...")
    parser.add_option(
        "-b",
        "--basedirectory",
        dest="basedir",
        help="directory where all the module data are",
        default="/Users/wouter/cernboxVeloModuleProduction/")
    parser.add_option("-s","--show", action="store_true", dest="show", default=False)
    parser.add_option("-f","--floatCToN", action="store_true", dest="floatCToN", default=False)
    parser.add_option("-c","--correctForDeflection", action="store_true", dest="correctForDeflection", default=False)
    parser.add_option("-u","--useSubstrateForCToN", action="store_true", dest="useSubstrateForCToN", default=False)
    parser.add_option("-v","--verbose",action="store_true", dest="verbose", default=False)
    (opts, args) = parser.parse_args()
    print("Number of args: %d" % len(args) )
    if len(args)<1:
        parser.error("incorrect number of arguments")
    from ReadFiles import readNikhefMetrologyData
    for m in args:
        moduledata = readNikhefMetrologyData(m, opts.basedir, opts.correctForDeflection)
        addTileXYTransforms(moduledata=moduledata,
                            useSubstrateForCToN = opts.useSubstrateForCToN,
                            floatCToN = opts.floatCToN, verbose =opts.verbose )

        measuredpoints = []
        for t in moduledata.tiledata.values() : measuredpoints += t.markerpoints
        drawdeltas( measuredpoints, figname = "deltaByTileToNominal_%s" % moduledata.name )
        drawdeltassingle( measuredpoints, figname = "deltaToNominal_%s" % moduledata.name )

    import matplotlib.pyplot as plt
    if opts.show: plt.show()

if __name__ == "__main__":
    main()


