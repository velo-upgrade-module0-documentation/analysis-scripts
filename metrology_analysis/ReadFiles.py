#no documentation. just run from the directory where the files are.
import re
import os

#this is sooooo shit
#import sys
#sys.path.append('/usr/local/lib/python3.8/site-packages')
from xlrd import open_workbook

from Coordinates3D import Point3D
import TileInfo

csideMarkerDictionary = {
    "asic marker 7" : "CLI_VP02_Fid2",
    "asic marker 11" : "CLI_VP00_Fid2",
    "asic marker 12" : "CLI_VP00_Fid1",
    "asic marker 1" : "CSO_VP30_Fid1",
    "asic marker 6" : "CSO_VP32_Fid2",
    "sensor marker 1" : "NSI_Sensor_Fid1",
    "sensor marker 2" : "NSI_Sensor_Fid2"
    }
    
nsideMarkerDictionary = {
    "asic marker 13" : "NLO_VP10_Fid1",
    "asic marker 18" : "NLO_VP12_Fid2",
    "asic marker 24" : "NSI_VP20_Fid1",
    "asic marker 23" : "NSI_VP20_Fid2",
    "asic marker 19" : "NSI_VP22_Fid2",
    "asic marker 12" : "NSI_VP20_Fid1",
    "asic marker 7" : "NSI_VP22_Fid2",
    "asic marker 6" : "NLO_VP10_Fid1",
    "asic marker 1" : "NLO_VP12_Fid2",
    "sensor marker 1" : "CLI_Sensor_Fid1",
    "sensor marker 2" : "CLI_Sensor_Fid2"
    }

def fromMartijnToLHCbFrame( point, isNSide ):
    # transformation from Martijn's frame to LHCb frame
    xpen = 275 ;
    ypen = 97 ;
    x = xpen -  point.y
    if not isNSide:
        y = -ypen - point.x
    else:
        y = -ypen + point.x
    return Point2D( x, y )
    
def extractMeasurementsFromWorkbook( filename, isNSide ):
    wb = open_workbook(filename)
    markers = {}
    
    for s in wb.sheets():
        markerpattern = re.compile(".*?(?P<coord>(X|Y)) Value_(?P<name>(ASIC|Sensor|Asic) Marker \d{1,2})")
    
        for row in range(s.nrows):
            matches = markerpattern.match(  str(s.cell(row,0).value) )
            if matches :
                name = matches.group('name').lower()
                value = s.cell(row,1).value
                if name not in markers.keys():
                  markers[name] = Point3D()
                if matches.group('coord')=='X':
                    markers[name].x = value
                elif matches.group('coord')=='Y':
                    markers[name].y = value
                else:
                    print("coord: ", matches.group('coord'))
                
            #else:
            #    print("no match: ", str(s.cell(row,0).value) )

    #print(markers)
    #for (key,value) in markers.items():
    #    print(key,value.x,value.y)

    # now translate them to the LHCb frame
    lhcbframemarkers = {}
    markerdictionary = csideMarkerDictionary
    if isNSide: markerdictionary = nsideMarkerDictionary
    for (wbname,wbpoint) in markers.items():
        if wbname in markerdictionary:
            name = markerdictionary[wbname]
            point = fromMartijnToLHCbFrame(wbpoint,isNSide)
            point.name = name
            point.title = name + " zeiss"
            lhcbframemarkers[name] = point
        else:
            print( "cannot find name in dictionary: ", wbname )
    print(lhcbframemarkers)
    return lhcbframemarkers

def readNikhefCSVFile( filename ):
    markers = None
    if os.path.isfile( filename ):
        markers = []
        f = open(filename)
        markerpattern = re.compile("(.*Fid.*)|(.*Sensor.*)|(MC.*)")
        for line in f:
            fields = line.split(';')
            if len(fields)>=3 and markerpattern.search(fields[0]):
                name=fields[0].strip()
                # correct for an unfortunate mistake in the P&P
                if   name=="MC_C_B4" : name = "MC_C_B1"
                elif name=="MC_C_B1" : name = "MC_C_B4"
                elif name=="MC_C_B2" : name = "MC_C_B3"
                elif name=="MC_C_B3" : name = "MC_C_B2"
                markers.append( Point3D(x=float(fields[1]),y=float(fields[2]),z=float(fields[3]),name=name,title=name+" p&p") )
            elif 'Marker' not in fields[0]:
                print("skipping: %s" % (fields[0]))
    else:
        print("Cannot find file: '%s'" %filename)
    return markers

def writeNikhefCSVFile( outputfilename, markerstowrite, measurements ):
    output = open(outputfilename,"w")
    output.write( "\"Marker\" ;\"X\" ;\"Y\" ;\"Z\"\n")
    cside = "CSide" in outputfilename    
    for m in markerstowrite:
        if m in measurements.keys():
            p = measurements[ m ]
            output.write( "%s ; %f ; %f ; %f \n" % (m, p.x, p.y, p.z) )
        else:
            print("cannot find marker '%s' in measurements" % m)
    output.close()

# class to store all metrology data for one module. we can also store fit results here
class ModuleMetrologyData:
    def __init__(self, name):
        self.name = name

class TileMetrologyData:
    def __init__(self, name):
        self.name = name
        self.markerpoints = []
        self.sensorpoints = []
        
def readNikhefMetrologyData( directory, basedir=None, correctforz=False, verbose=False ):
    modulename = "N999"
    if not os.path.isdir(directory):
        # assume that user provided a module name
        modulename = directory
        if not basedir: basedir = "/Users/wouter/cernboxVeloModuleProduction/"
        directory = basedir + modulename + "/tile_glueing/metrology/"
    else:
        modulenamepattern = re.compile(".*?(?P<name>N\d\d\d).*?")
        result = modulenamepattern.search( directory )
        if result: modulename = result.group('name')
            

    # read the tile marker measurements
    moduledata = ModuleMetrologyData(name=modulename)
    moduledata.csidetilepoints = readNikhefCSVFile( directory + "CSideTilePositions.csv" )
    moduledata.nsidetilepoints = readNikhefCSVFile( directory + "NSideTilePositions.csv" )
    moduledata.csidesubstratepoints = readNikhefCSVFile( directory + "CSideSubstrateSurface.csv" )
    moduledata.nsidesubstratepoints = readNikhefCSVFile( directory + "NSideSubstrateSurface.csv" )
    moduledata.clisensorpoints = readNikhefCSVFile( directory + "CLISensorSurface.csv" )
    moduledata.csosensorpoints = readNikhefCSVFile( directory + "CSOSensorSurface.csv" )
    moduledata.nsisensorpoints = readNikhefCSVFile( directory + "NSISensorSurface.csv" )
    moduledata.nlosensorpoints = readNikhefCSVFile( directory + "NLOSensorSurface.csv" )

    # apply a correction for the tilt of the focus axis. this is such a small effect, that we perhaps should leave it out.
    DXDZ = +0.0020
    for d in [ moduledata.csidetilepoints, moduledata.csidesubstratepoints, moduledata.clisensorpoints, moduledata.csosensorpoints,
               moduledata.nsidetilepoints, moduledata.nsidesubstratepoints, moduledata.nsisensorpoints, moduledata.nlosensorpoints,]:
        if d:
            for p in d: p.m[0] += DXDZ * p.m[2] 
    
    # swap the z positions for all C-side measurements
    for d in [ moduledata.csidetilepoints, moduledata.csidesubstratepoints, moduledata.clisensorpoints, moduledata.csosensorpoints]:
        if d:
            for p in d: p.m[2] *= -1

    # reorganize the data a bit such that we can access by tile
    tilenames = [ "CLI","CSO","NLO","NSI" ]
    moduledata.tiledata = { n : TileMetrologyData(n) for n in tilenames }
    moduledata.tiledata["CLI"].sensorpoints = moduledata.clisensorpoints
    moduledata.tiledata["CSO"].sensorpoints = moduledata.csosensorpoints
    moduledata.tiledata["NLO"].sensorpoints = moduledata.nlosensorpoints
    moduledata.tiledata["NSI"].sensorpoints = moduledata.nsisensorpoints
    for p in (moduledata.csidetilepoints + moduledata.nsidetilepoints):
        for n in tilenames:
            if n in p.name: moduledata.tiledata[n].markerpoints.append(p)

    # compute the Z corrections
    from FitPlane import addTileZAlignmentTransforms
    if moduledata.clisensorpoints and moduledata.nsisensorpoints :
        addTileZAlignmentTransforms(moduledata,verbose=verbose)
        from FitPlane import drawTileZResiduals
        drawTileZResiduals(moduledata)
        
    from AnalyseSubstrateMarkerPositions import addSubstrateCToNTransform
    addSubstrateCToNTransform(moduledata,verbose = verbose)

    from AnalyseTilePositions import addTileXYTransforms
    addTileXYTransforms(moduledata,useSubstrateForCToN=True,verbose=verbose)
    
    # compute the XY corrections
                
    # experimental: let's correct the points for curvature. this still misses a proper formalism
    #if correctforz:
    #    for (points,plane) in [ (moduledata.csidetilepoints,moduledata.csidesubstratefit),(moduledata.nsidetilepoints,moduledata.nsidesubstratefit) ] :
    #        for p in points:
    #            # we apply the correction that would put the substrate at z=0. 
    #            dz    = plane.z( p.x(), p.y() )
    #            deriv = plane.derivative( p.x(), p.y() )
    #            print("deriv: ", deriv )
    #            p.m[0,0] += 0.5*dz*deriv[0]
    #            p.m[1,0] += 0.5*dz*deriv[1]
                
    # make one plot for z-residuals (cannot we combine them all?)
    return moduledata


# global structure to keep all module data such that we can plot things for different modules
moduledatacatalog = {}

if __name__ == "__main__":
    from optparse import OptionParser
    parser = OptionParser(usage="%prog [options] <module name> ...")
    parser.add_option("-b","--basedirectory",dest="basedir",help="directory where all the module data are",
        default="/Users/wouter/cernboxVeloModuleProduction/")
    parser.add_option("-v","--verbose")
    (opts, args) = parser.parse_args()
    print("Number of args: %d" % len(args) )
    if len(args)<1:
        parser.error("incorrect number of arguments")

    for i in args:
        moduledata = readNikhefMetrologyData( i, opts.basedir)
        moduledatacatalog[moduledata.name] = moduledata

    # create a table with the alignment parameters for all tiles
    for data in moduledatacatalog.values():
        for t in data.tiledata.values():
            deltaZ  = t.DeltaZTransform
            deltaXY = t.DeltaXYTransform
            delta   = deltaZ*deltaXY
            #delta = deltaZ
            print(data.name, t.name, delta )

            # compute the deviation from nominal glue thickness at the four sensor points in two different ways
            tileinfo =  TileInfo.tileinfodict[t.name]
            sensorcornersLocal = TileInfo.TileInfo.corners()
            for corner in sensorcornersLocal:
                # method A: get the global z position with nominal and with aligned transform
                globalposnominal = tileinfo.transform()*corner
                globalposaligned = tileinfo.transform()*delta*corner
                deltazA = globalposaligned.z() - globalposnominal.z()
                # method B: take the substrate and sensor fits and get the difference
                substratefit    = moduledata.nsidesubstratefit
                nominaldistance = 0.49
                if "C" in t.name:
                    substratefit = moduledata.csidesubstratefit
                    nominaldistance *= -1
                #for i in range(3,6): substratefit.pars[i] = 0.0
                x = globalposnominal.x()
                y = globalposnominal.y()
                deltazB = t.sensorsurfacefit.z(x,y)  - substratefit.z(x,y) - nominaldistance
                #print( globalposnominal )
                print( data.name, t.name, deltazA, deltazB, t.sensorsurfacefit.z(x,y), substratefit.z(x,y) )
        print( data.name, "substrate position: ",#data.nsidesubstratefit.pars[0],data.csidesubstratefit.pars[0],
                   0.5*(data.nsidesubstratefit.pars[0]+data.csidesubstratefit.pars[0]),
                   0.5*(data.nsidesubstratefit.pars[1]+data.csidesubstratefit.pars[1]),
                   0.5*(data.nsidesubstratefit.pars[2]+data.csidesubstratefit.pars[2]) )
    print("So the problem is the 'constrained' fit used to derive the transform. That makes the difference in glue thickness really larger. Shall we get rid of it?")
    dwdwqdwq
            
            
    # create a histogram for the distance between tile markers
    distances = []
    from TileInfo import SENSOR_DIST_FIRST_LAST_PIXEL as nominaldistance
    for data in moduledatacatalog.values():
        for t in data.tiledata.values():
            n = len(t.markerpoints)
            for i in range(0,n):
                pi = t.markerpoints[i]
                if "VP" in pi.name:
                    for j in range(0,i):
                        pj = t.markerpoints[j]
                        if pi.name[0:7] == pj.name[0:7]:
                            dx = pi.x() - pj.x() 
                            dy = pi.y() - pj.y()
                            from math import sqrt
                            d  = sqrt(dx**2 + dy**2)
                            #print(pi.name, pj.name,d-nominaldistance)
                            if abs(d-nominaldistance)<1: distances.append( d )
    
    import matplotlib.pyplot as plt
    import numpy as np
    fig, ax1 = plt.subplots(1,1)
    ax1.set_xlabel('marker distance [mm]')
    plt.rcParams.update({'figure.figsize':(7,5), 'figure.dpi':100})
    residuals = [ (d-nominaldistance) for d in distances ]
    ax1.hist(distances, bins=40, range=(nominaldistance-0.02,nominaldistance+0.02) )
    #ax1.hist(residuals, bins=40, range=(-0.02,+0.02) )
    mean = np.mean(residuals)
    rms  = np.sqrt(np.mean(np.square(np.subtract(residuals,mean))))
    ax1.text(nominaldistance + 0.01,0.5,"rms:  %6.4f" % (rms))
    ax1.text(nominaldistance + 0.01,0.7,"mean: %6.4f" % (mean))
    #ax1.set_title(moduledata.name)
    fig.savefig('markerdistances.pdf')

    # create histogram for the CToN transform parameters
    print(len(moduledatacatalog.values()))
    for i in range(0,3):
        par0 = [ data.SubstrateCToN.parameters[i] for data in moduledatacatalog.values() ]
        err0 = [ sqrt(data.SubstrateCToN.covariance[i,i]) for data in moduledatacatalog.values() ]
        print("par",par0,err0)
