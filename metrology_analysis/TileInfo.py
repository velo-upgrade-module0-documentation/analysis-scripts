#import Coordinates
import Coordinates3D as Coordinates
from Coordinates3D import Point3D
import math

# all the constants that define the tile geometry

#tile_width  = 43.47 #mm DO NOT USE
#tile_height = 17.04 #mm DO NOT USE

PIXEL_SIZE = 0.055
LONG_PIXEL_SIZE = (0.3300 - PIXEL_SIZE)/2
SENSOR_DIST_FIRST_LAST_PIXEL = (3*256-1) * PIXEL_SIZE + 4*(LONG_PIXEL_SIZE-PIXEL_SIZE)

SENSOR_WIDTH  = 43.470
SENSOR_HEIGHT = 14.980
SENSOR_MARKER_TO_SENSOR_SIDE_SHORT = 0.09
SENSOR_MARKER_TO_SENSOR_SIDE_LONG  = 0.64
SENSOR_EDGE_TO_ASIC_MARKER = 1.853

SENSOR_FIRST_PIXEL_CENTER_TO_SENSOR_BOTTOM = (SENSOR_HEIGHT - 255*PIXEL_SIZE)/2
SENSOR_FIRST_PIXEL_CENTER_TO_SENSOR_SIDE   = (SENSOR_WIDTH  - SENSOR_DIST_FIRST_LAST_PIXEL)/2

ASIC_WIDTH  = 14.14 # width (x) in mm
ASIC_HEIGHT = 16.6  # height (y) in mm
ASIC_BOTTOM_TO_SENSOR_BOTTOM  = 1.970

#ASIC_DISTANCE = 2*LONG_PIXEL_SIZE - 3*PIXEL_SIZE
ASIC_INTERVAL = 256*PIXEL_SIZE + 2*(LONG_PIXEL_SIZE-PIXEL_SIZE)
ASIC_DISTANCE = 0.105 # distance in x between two asics (nominal, missing 'extra material')
ASIC_WIDTH    = ASIC_INTERVAL - ASIC_DISTANCE
ASIC_FIRST_PIXEL_CENTER_TO_ASIC_SIDE = (ASIC_WIDTH - 255*PIXEL_SIZE)/2
ASIC_MARKER_TO_ASIC_SIDE   = 0.058
ASIC_MARKER_TO_ASIC_BOTTOM = 0.117

# The tile frame of reference has its origin in the bottom-left
# pixel. The following are marker positions in that system
#TILE_MARKER1_X = - (ASIC_FIRST_PIXEL_CENTER_TO_ASIC_SIDE - ASIC_MARKER_TO_ASIC_SIDE)
#TILE_MARKER1_Y = - (ASIC_BOTTOM_TO_SENSOR_BOTTOM + SENSOR_FIRST_PIXEL_CENTER_TO_SENSOR_BOTTOM-ASIC_MARKER_TO_ASIC_BOTTOM)
#TILE_MARKER6_X =   (SENSOR_DIST_FIRST_LAST_PIXEL + ASIC_FIRST_PIXEL_CENTER_TO_ASIC_SIDE - ASIC_MARKER_TO_ASIC_SIDE)
#TILE_MARKER6_Y = TILE_MARKER1_Y
# I take these now from the ASIC drawing
TILE_MARKER1_X = 0
TILE_MARKER2_X = TILE_MARKER1_X + 255*PIXEL_SIZE
TILE_MARKER3_X = TILE_MARKER2_X + 2*LONG_PIXEL_SIZE - PIXEL_SIZE
TILE_MARKER4_X = TILE_MARKER3_X + 255*PIXEL_SIZE
TILE_MARKER5_X = TILE_MARKER4_X + 2*LONG_PIXEL_SIZE - PIXEL_SIZE
TILE_MARKER6_X = TILE_MARKER5_X + 255*PIXEL_SIZE
TILE_MARKER_Y = -2.33043

# These are constants needed for the 3D transforms for the 4 tiles. These come from Tom
CLOSEST_INNER_PIXEL_ROW    = 93*PIXEL_SIZE
CLOSEST_OUTER_PIXEL_ROW    = 343*PIXEL_SIZE
CLOSEST_SHORT_PIXEL_COLUMN = 95*PIXEL_SIZE
CLOSEST_NLO_PIXEL_COLUMN   = 73*PIXEL_SIZE
CLOSEST_CLI_PIXEL_COLUMN   = 98*PIXEL_SIZE
SI_SIZE_X                  = 3*(256*PIXEL_SIZE)+2*0.165 # this one I do not understand, but in the end it is the same as mine
SI_SIZE_Y                  = 256*PIXEL_SIZE

TILE_THICKNESS  = 0.225
BOND_THICKNESS  = 0.020
SENSOR_THICKNESS = 0.2
SubstrateThick = 0.5
GlueThick = 0.07
VPRotation = math.pi/4

# unfortunately, the local coordinate system is still wrong by half a pixel.
# (everythng below is consistent, but it is not consistent with the LHCb geometry)

NLO_FIRSTPIXEL_LOCALX =  -73*PIXEL_SIZE
NLO_FIRSTPIXEL_LOCALY = -598*PIXEL_SIZE
NSI_FIRSTPIXEL_LOCALX =  -95*PIXEL_SIZE
NSI_FIRSTPIXEL_LOCALY = -348*PIXEL_SIZE
CLI_FIRSTPIXEL_LOCALX = -675*PIXEL_SIZE
CLI_FIRSTPIXEL_LOCALY = -348*PIXEL_SIZE
CSO_FIRSTPIXEL_LOCALX = -678*PIXEL_SIZE
CSO_FIRSTPIXEL_LOCALY = -598*PIXEL_SIZE

def csoTransform():
  transform =  Coordinates.rotationY( math.pi ) * Coordinates.rotationZ( math.pi * -1./4. ) * \
    Coordinates.translation3D( CSO_FIRSTPIXEL_LOCALX, CSO_FIRSTPIXEL_LOCALY, SubstrateThick/2+GlueThick )
  transform.name = "CSOTransform"
  return transform

def cliTransform():
  transform =  Coordinates.rotationY( math.pi ) * Coordinates.rotationZ( math.pi * -3./4. ) * \
    Coordinates.translation3D( CLI_FIRSTPIXEL_LOCALX, CLI_FIRSTPIXEL_LOCALY, SubstrateThick/2+GlueThick )
  transform.name = "CLITransform"
  return transform
  
def nloTransform():
    transform = Coordinates.rotationZ( math.pi * 3./4. ) * \
    Coordinates.translation3D( NLO_FIRSTPIXEL_LOCALX, NLO_FIRSTPIXEL_LOCALY, SubstrateThick/2+GlueThick )
    transform.name = "NLOTransform"
    return transform

def nsiTransform():
    transform = Coordinates.rotationZ( math.pi * 1./4. ) * \
    Coordinates.translation3D( NSI_FIRSTPIXEL_LOCALX, NSI_FIRSTPIXEL_LOCALY, SubstrateThick/2+GlueThick )
    transform.name = "NSITransform"
    return transform

class TileInfo:
    __slots__ = [ "name", "NominalTransform" ]
    def __init__(self, name, transform ) :
        self.name = name
        self.NominalTransform = transform

    def tilemarkers(self):
        tilemarkerprefix = ""
        if self.name=="CLI": tilemarkerprefix = "CLI_VP0"
        if self.name=="CSO": tilemarkerprefix = "CSO_VP3"
        if self.name=="NSI": tilemarkerprefix = "NSI_VP2"
        if self.name=="NLO": tilemarkerprefix = "NLO_VP1"

        rc = []
        for m in [ ("0_Fid1",TILE_MARKER1_X,TILE_MARKER_Y),
                        ("0_Fid2",TILE_MARKER2_X,TILE_MARKER_Y),
                        ("1_Fid1",TILE_MARKER3_X,TILE_MARKER_Y),
                        ("1_Fid2",TILE_MARKER4_X,TILE_MARKER_Y),
                        ("2_Fid1",TILE_MARKER5_X,TILE_MARKER_Y),
                        ("2_Fid2",TILE_MARKER6_X,TILE_MARKER_Y) ] :
            rc.append( Coordinates.Point3D( name = tilemarkerprefix+m[0], x = m[1], y = m[2], z=TILE_THICKNESS ) )
        return rc

    def markers(self):
        rc = self.tilemarkers()
        sensorcornermarkers =  [ ("S3",0.1625,  255*PIXEL_SIZE + 0.3875),
                                 ("S4",-0.3875, 255*PIXEL_SIZE - 0.1625),
                                 ("S5",SENSOR_DIST_FIRST_LAST_PIXEL -0.1625, 255*PIXEL_SIZE + 0.3875),
                                 ("S6",SENSOR_DIST_FIRST_LAST_PIXEL +0.3875, 255*PIXEL_SIZE - 0.1625),
                                     ("S2",-0.3875,0.1625),
                                     ("S7",SENSOR_DIST_FIRST_LAST_PIXEL+0.3875,0.1625),
                                     ("S8",SENSOR_DIST_FIRST_LAST_PIXEL-0.1625,-0.3875),
                                     ("T40",SENSOR_DIST_FIRST_LAST_PIXEL-39*PIXEL_SIZE,255*PIXEL_SIZE + 0.3875),
                                     ("L230",SENSOR_DIST_FIRST_LAST_PIXEL +0.3875,229*PIXEL_SIZE),
                                     ("T610",(3*256-610)*PIXEL_SIZE,255*PIXEL_SIZE + 0.3875),
                                     ("R30",-0.3875,29*PIXEL_SIZE)]
        for m in sensorcornermarkers:
            rc.append( Coordinates.Point3D( name = self.name + "_Sensor_" + m[0], x = m[1], y = m[2], z=TILE_THICKNESS + BOND_THICKNESS ) )
        return rc
    def transform(self):
        return self.NominalTransform #* self.DeltaTransform
    def markersInGlobalFrame(self):
        t = self.transform()
        return [ t*p for p in self.markers()]

    def isNSide(self) :
        return "N" in self.name
    def cornerpixels():
        x1 = SENSOR_DIST_FIRST_LAST_PIXEL
        y1 = 255*PIXEL_SIZE
        return [ Point3D(0,0,0,"rear left"),Point3D(x1,0,0,"rear right"),
            Point3D(x1,y1,0,"front right"),Point3D(0,y1,0,"front left") ]
    def asiccorners(iasic):
        x0array = [ TILE_MARKER1_X, TILE_MARKER3_X, TILE_MARKER5_X ]
        x1 = x0array[iasic] - ASIC_MARKER_TO_ASIC_SIDE
        y1 = TILE_MARKER_Y  - ASIC_MARKER_TO_ASIC_BOTTOM
        x2 = x0array[iasic] + ASIC_MARKER_TO_ASIC_SIDE + 255*PIXEL_SIZE
        y2 = y1 + ASIC_HEIGHT
        return [ Point3D(x1,y1,0,"ASICCORNER1"),Point3D(x2,y1,0,"ASICCORNER2"),
            Point3D(x2,y2,0,"ASICCORNER3"),Point3D(x1,y2,0,"ASIXCORNER4") ]
    def sensorSurfaceCentre(self):
        return Coordinates.Point3D( 0.5*SENSOR_DIST_FIRST_LAST_PIXEL,0.5*255*PIXEL_SIZE, TILE_THICKNESS + BOND_THICKNESS + SENSOR_THICKNESS )

    
tileinfos = [ TileInfo("NLO",nloTransform()), TileInfo("NSI",nsiTransform()), \
              TileInfo("CLI",cliTransform()), TileInfo("CSO",csoTransform()),  ]
tileinfodict = { t.name : t for t in tileinfos }
tilenames = list(tileinfodict.keys())

# backward compatibility
anominalmap = {"CLI_Sensor_Fid1":"CLI_Sensor_S3",
               "CLI_Sensor_Fid2":"CLI_Sensor_S4",
               "NSI_Sensor_Fid1":"NSI_Sensor_S3",
               "NSI_Sensor_Fid2":"NSI_Sensor_S4",
               "CSO_Sensor_Fid3":"CSO_Sensor_S7"}
TileMarkers = { p.name : p for t in tileinfos for p in t.markersInGlobalFrame() }
for m in anominalmap: TileMarkers[ m ] = TileMarkers[ anominalmap[m] ]

def sensorCentreOfGravity():
  sumx = 0.
  sumy = 0.
  sumz = 0.
  for t in tileinfos :
      p = t.transform() * t.sensorSurfaceCentre()
      sumx += p.x()
      sumy += p.y()
      sumz += p.z()
  return Coordinates.Point3D(name = "module centre", x = sumx/4, y=sumy/4, z=sumz/4)

def tileTransformLatexTable():
    print("name & $v_x$ & $v_y$ & $v_z$ & $\\theta_x$ & $\\theta_y$ & $\\theta_z$ & $p_x$ & $p_y$ & $p_z$ \\\\")
    for ti in tileinfos:
        transform = ti.NominalTransform
        vecv   = transform.translation
        matrix = transform.rotation
        vecp   = matrix.dot( vecv )
        rot    = transform.rotationParameters()
        PI = math.pi
        print( "%s & %7.4f & %7.4f & %7.4f & %7.2f & %7.2f & %7.2f & %7.3f & %7.3f & %7.3f \\\\" % \
                (ti.name,\
                vecv[0], vecv[1], vecv[2],\
                rot[2]/PI, rot[1]/PI, rot[0]/PI,
                vecp[0], vecp[1], vecp[2] ) )

#ASIC_FIRST_LAST_MARKER_DIST = ASIC_WIDTH - 2*ASIC_MARKER_TO_ASIC_SIDE

if __name__ == "__main__":
  print("LONG_PIXEL_SIZE                           : %5.4f" % LONG_PIXEL_SIZE)
  print("SENSOR_FIRST_PIXEL_CENTER_TO_SENSOR_BOTTOM: %5.4f" % SENSOR_FIRST_PIXEL_CENTER_TO_SENSOR_BOTTOM)
  print("SENSOR_FIRST_PIXEL_CENTER_TO_SENSOR_SIDE  : %5.4f" % SENSOR_FIRST_PIXEL_CENTER_TO_SENSOR_SIDE)
  print("SENSOR_DIST_FIRST_LAST_PIXEL              : %5.4f" % SENSOR_DIST_FIRST_LAST_PIXEL)
  print("ASIC_DISTANCE                             : %5.4f" % ASIC_DISTANCE)
  print("ASIC_INTERVAL                             : %5.4f" % ASIC_INTERVAL)
  print("ASIC_WIDTH                                : %5.4f" % ASIC_WIDTH)
  print("ASIC_FIRST_PIXEL_CENTER_TO_ASIC_SIDE      : %5.4f" % ASIC_FIRST_PIXEL_CENTER_TO_ASIC_SIDE)
  print("TILE_MARKER1_X                            : %5.4f" % TILE_MARKER1_X)
  print("TILE_MARKER6_X                            : %5.4f" % TILE_MARKER6_X)
  print("TILE_MARKER_Y                             : %5.4f" % TILE_MARKER_Y)
  print("SI_SIZE_X                                 : %5.4f" % SI_SIZE_X)

  print("Sensors centre of gravity in module frame: ", sensorCentreOfGravity() )
  
  print("Markers in global frame: ")
  markerdict = {}
  for t in tileinfos :
    for m in t.markersInGlobalFrame():
      print(m)
      markerdict[m.name] = m

  anominalmap = {"CLI_Sensor_Fid1":"CLI_Sensor_S3",
                 "CLI_Sensor_Fid2":"CLI_Sensor_S4",
                 "NSI_Sensor_Fid1":"NSI_Sensor_S3",
                 "NSI_Sensor_Fid2":"NSI_Sensor_S4",
                 "CSO_Sensor_Fid3":"CSO_Sensor_S7"}

  import NominalMarkerPositions
  for m in NominalMarkerPositions.TileMarkersInDrawings:
    pos = NominalMarkerPositions.TileMarkersInDrawings[m]
    name = m
    if m not in markerdict:
      name = anominalmap[m]
    mprime = markerdict[name]
    print("%s %6.5f % 6.5f %6.5f %6.5f" % (m,mprime.x(), mprime.y(), pos[0]-mprime.x(), pos[1]-mprime.y()) )
    #for k in tiletransforms.keys():
    #  if k in m: t = tiletransforms[k]
    #pos = NominalMarkerPositions.TileMarkers[m]
    #p = Coordinates.Point3D(x=pos[0],y=pos[1],z=0)
    #print(m, pos, "-->", t.inverse()*p)

  tileTransformLatexTable()
  
  print("Pixel positions in global frame")
  for t in tileinfos:
      for p in TileInfo.cornerpixels():
          pglobal = t.transform() * p
          print("%s %s %6.4f %6.4f" %(t.name, "{:<12}".format(p.name), pglobal.x(), pglobal.y()))
    
 
