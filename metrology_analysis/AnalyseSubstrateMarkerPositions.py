# This scripts makes delta-x/delta-y plots for the markers for one file or one module
# function that draws the deltas. this could go to another module at some point


def findhistomax( data, bins, arange ):
    import numpy
    hist, bin_edges = numpy.histogram( data, bins = bins, range=arange )
    imax = 0
    for i in range(bins):
        if hist[imax] < hist[i]: imax = i
    # compute a weighted average from the three bins. (see PV finding note)
    delta = 0.5*( hist[imax+1] - hist[imax-1] )/(2.0*hist[imax] - hist[imax+1] - hist[imax-1] )
    return (1-delta)*bin_edges[imax] + delta*bin_edges[imax+1]

def fitSubstrateTransforms( measuredpoints, modulename=None, verbose=False ):
    import numpy as np
    # let's extract dx, dy and dphi from the measured values. since N and C
    # are actually independent, perhaps I should split the routine in two.
    transformnames = ['CSideSubstrate','NSideSubstrate']
    from Coordinates import AccumulatorForParametrizedTransform2Ds, Point2D
    accumulator = AccumulatorForParametrizedTransform2Ds( transformnames )
    
    #from Coordinates import Accumulator
    #accumulator = Accumulator( parnames )
    skippedmarkers = ['MC_NSI_Fid3'] # ['MC_N21_NSI_Fid1']

    # I would like to plot residuals. just combine x and y
    xresiduals = []
    yresiduals = []
    from NominalMarkerPositions import XYRESOLUTION
    
    for iteration in range(0,4):
        accumulator.reset()
        xresiduals.clear()
        yresiduals.clear()

        # obtain the current transforms (requires some computation, so rather do once per iteration)
        csidetransform = accumulator.transform('CSideSubstrate')
        nsidetransform = accumulator.transform('NSideSubstrate')

        # we are thrown off by a combination of a really bad
        # alignment, and a few bad points in the first iteration. In
        # the end I solved this by using the adaptive fit from the
        # start with an 'annealing scheme', ie a changing chi2max.
        
        for i in measuredpoints:
            if i.name in skippedmarkers: continue
            from NominalMarkerPositions import SubstrateMarkers
            nominalposition = None
            for (key,value) in SubstrateMarkers.items():
                if key in i.name:
                    nominalposition = value
            if not nominalposition:
                print("cannot find nominal position for: ", i.name )
            else:
                xnominal = nominalposition[0]
                ynominal = nominalposition[1]
                
                pnominal= Point2D( x=xnominal,y=ynominal,name = key )
                # let's first do this in the global frame
                isC = 'MC_C' in i.name
                if isC:
                    transform     = csidetransform
                    transformname = "CSideSubstrate"
                else:
                    transform     = nsidetransform 
                    transformname = "NSideSubstrate"

                pnominal_aligned = transform * pnominal
                residualX = i.x() - pnominal_aligned.x()
                residualY = i.y() - pnominal_aligned.y()
                
                sigma2 = XYRESOLUTION**2
                weight = 1.0
                # compute a Tukey weight = ( 1 - chi2/maxchi2)^2
                chi2   = (residualX*residualX+residualY*residualY)/sigma2
                chi2max = [100000,1000,25,25,25,25]
                weight = 0.0
                eta = chi2/chi2max[iteration]
                if eta<1 : weight = (1 - eta)**2
                                
                if not i.name in skippedmarkers:
                    HX = np.zeros( (3,1), np.float64 )
                    HX[0,0] = -1
                    HX[2,0] = + pnominal_aligned.y()
                    HXdict = { transformname : HX }
                    accumulator.add( residual=residualX, weight=weight/sigma2, H=HXdict )

                    HY = np.zeros( (3,1), np.float64 )
                    HY[1,0] = -1
                    HY[2,0] = - pnominal_aligned.x()
                    HYdict = { transformname : HY }
                    accumulator.add( residual=residualY, weight=weight/sigma2, H=HYdict )

                if verbose:
                    # some markers are off. let's see where we think they are:
                    pmeasured = transform.inverse() * Point2D( i.x(), i.y() )
                    drawingx = - pmeasured.y() + 70
                    drawingy = - pmeasured.x() + 90
                    print("name, dx, dy, x, y, weight: %s %5.3f %5.3f %5.3f %5.3f %5.3f" % (i.name,residualX,residualY,drawingx,drawingy,weight) )
                if weight>0.001:
                    xresiduals.append( residualX )
                    yresiduals.append( residualY )
                
        # now solve
        accumulator.update()
        if verbose: print("chi2, ndof: ", accumulator.chi2, accumulator.ndof)
        if verbose: print("pars: ", accumulator.parameters)
        
        #print("%f %f %f %f" % (i.x(), i.y(), xnominal, ynominal) )

    #print a table with the result
    if verbose:
        for n in transformnames:
            print( accumulator.parametrizedTransform(n) )

    #for i in range(accumulator.dim):
    #    print("%s & %f \\\\" % (accumulator.parnames[i], accumulator.parameters[i] ) )
    # derive the transform between the two sets of parameters
    csidetransform = accumulator.transform('CSideSubstrate')
    nsidetransform = accumulator.transform('NSideSubstrate')
    csidetransform = accumulator.parametrizedTransform('CSideSubstrate')
    nsidetransform = accumulator.parametrizedTransform('NSideSubstrate')
    
    # plot a histogram of the residuals
    import matplotlib.pyplot as plt
    residualsforplots = { "x" : xresiduals, "y" : yresiduals }
    for a in residualsforplots:
        residuals = residualsforplots[a]
        fig, ax1 = plt.subplots(1,1)
        ax1.set_xlabel('%s residual [mm]' % a)
        if modulename: ax1.set_title(modulename)
        plt.rcParams.update({'figure.figsize':(7,5), 'figure.dpi':100})
        truncatedresiduals = [r for r in residuals if abs(r)<0.05 ]
        rms = np.sqrt(np.mean(np.square(truncatedresiduals)))
        ax1.hist(residuals, bins=40, range=(-0.02,0.02) )
        ax1.text(0.01,3,"rms: %6.4f" % rms)
        if modulename: fig.savefig('substrate%sresiduals_%s.pdf'%(a,modulename))
        else:          fig.savefig('substrate%sresiduals.pdf' % a)
    plt.close()

    return (nsidetransform,csidetransform)
    
def addSubstrateCToNTransform( moduledata, verbose=False ):
    from Coordinates import Transform2D
    moduledata.SubstrateCToN = Transform2D()
    if moduledata.csidesubstratepoints and moduledata.nsidesubstratepoints:
        # we need to split the routine above. that makes it much easier to understand.
        substratepoints = moduledata.csidesubstratepoints + moduledata.nsidesubstratepoints
        (nsidetransform,csidetransform) = fitSubstrateTransforms( substratepoints, moduledata.name, verbose=verbose)
        moduledata.SubstrateTransformNSide = nsidetransform
        moduledata.SubstrateTransformCSide = csidetransform
        moduledata.SubstrateCToN = nsidetransform.inverse() * csidetransform
        moduledata.SubstrateCToN.name = "SubstrateCToN"
        if verbose: print("fromCToN:", moduledata.SubstrateCToN)
 
# the main function
if __name__ == "__main__":
    import numpy
    from optparse import OptionParser
    parser = OptionParser(usage="%prog [options] <file or directory or module name> ...")
    parser.add_option("-b","--basedirectory",dest="basedir",help="directory where all the module data are",
        default="/Users/wouter/cernboxVeloModuleProduction/")
    parser.add_option("-v","--verbose",action="store_true", dest="verbose", default=False)
    parser.add_option("-s","--show", action="store_true", dest="show", default=False)
    (opts, args) = parser.parse_args()
    print("Number of args: %d" % len(args) )
    if len(args)<1:
        parser.error("incorrect number of arguments")
        
    # create the list of files from which we extract measurements
    from ReadFiles import readNikhefMetrologyData
    for m in args:
        moduledata = readNikhefMetrologyData(m,opts.basedir)
        measuredpoints = moduledata.csidesubstratepoints + moduledata.nsidesubstratepoints
        fitSubstrateTransforms( measuredpoints, moduledata.name, verbose=opts.verbose)
        print(moduledata.name,moduledata.SubstrateTransformNSide)
        print(moduledata.name,moduledata.SubstrateTransformCSide)

    if opts.show:
        import matplotlib.pyplot as plt
        plt.show()

# we temporarily put this here. this routine takes
# *  positions of markers of the tiles
# *  positions of markers on the substrate
#
# it should then derive alignment constants, using the substrate to link front and back. the parameters are:
# * dx,dy,phi for each tile in LHCb frame
# * dx,dy,phi of substrate in LHCb frame

# the first thing we should do is see if the substrate measurements
# are reliable enough: we may need to remove some outliers. so, let's
# analyse the substrate markers first.

# We need to decide in which 'frame' we compute the constants. I don't
# want to do the complicated thing with transporting  transform
# derivatives to different frames. There are then two kinds of solution:

# 1. compute all delta transforms in the global frame. Perhaps we
# should then introduce picot points for the rotations in order to
# reduce correlations. On the other hands, with double precision we
# should be fine.

# 2. compute the delta transforms in the tile frame. In this case, we
# compute residuals also in the tile frame. One advantage is that we
# more easily introduce parameters for the small displacement of the
# asics relative to the sensor. Another advantage is that if we use
# the right frame, we can immeddiately use the result in LHCb.

# In any case, you see that it would be good to work with transforms.
