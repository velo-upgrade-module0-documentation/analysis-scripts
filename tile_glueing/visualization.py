

# Below are some functions implemented to draw the output of the
# patterns on top of the ASICs such that you can see if your pattern
# makes sense.
from geometry import Point2D, Transform2D
def drawTileOutline(ax, tile, transform = Transform2D() ):
    import numpy as np
    import matplotlib.pyplot as plt
    from matplotlib.patches import Polygon
    from geometry import asic_interval, asic_width, asic_height
    
    # draw all three asic separately
    for iasic in range(0,3):
        # four corner points in tile frame
        p1 = Point2D(0 + iasic*asic_interval,0)
        p2 = Point2D(p1.x() + asic_width, p1.y())
        p3 = Point2D(p1.x() + asic_width, p1.y()+asic_height)
        p4 = Point2D(p1.x(), p1.y()+asic_height)
        # transform to global frame
        T = transform * tile.transform()
        p1p = T * p1
        p2p = T * p2
        p3p = T * p3
        p4p = T * p4
        points = [ p1p, p2p, p3p, p4p ]
        pointpositions = [ [ p.x(), p.y() ] for p in points ]
        pts = np.array( pointpositions )
        poly = Polygon(pts, closed=True,fill=False)
        ax.add_patch(poly)
        # add some thin dashed lines to show the center of the tile
        clps = [ Point2D(p1.x() + asic_width/2, p1.y()),
                 Point2D(p1.x() + asic_width/2, p1.y() + asic_height),
                 Point2D(p1.x(), p1.y() + asic_height/2),
                 Point2D(p1.x() + asic_width, p1.y() + asic_height/2) ]
        clpsp = [ T*p for p in clps ]
        plline = plt.Line2D( (clpsp[0].x(),clpsp[1].x()),(clpsp[0].y(),clpsp[1].y()),lw=0.5,linestyle='--',c='black')
        ax.add_line(plline)
        plline = plt.Line2D( (clpsp[2].x(),clpsp[3].x()),(clpsp[2].y(),clpsp[3].y()),lw=0.5,linestyle='--',c='black')
        ax.add_line(plline)
        #ax.update_datalim( [ ( p.x(), p.y() ) for p in clpsp ] )
        
    # add the markers
    markers = [ transform * tile.FirstMarker, transform * tile.LastMarker]
    for m in markers:
        ax.scatter( m.x(), m.y(), c="red", marker="*" )
    
def drawPattern(ax, filename, index = 1 ):
    import matplotlib.pyplot as plt
    inputfile = open(filename, "r")
    x = 0.
    y = 0.
    lineactive = False
    import matplotlib.pyplot as plt
    for line in inputfile.readlines():
        linestart   = "Line Start" in line
        linepassing = "Line Passing" in line
        lineend     = "Line End" in line
        dispensedot = "Dispense Dot" in line
        if linestart or linepassing or lineend or dispensedot:
            xold = x
            yold = y
            words = line.split(",")
            x = float(words[1])
            y = float(words[2])
            ax.update_datalim( [ (x,y) ] )
        if linestart: lineactive = True
        if linepassing or lineend:
            if lineactive: 
               plline = plt.Line2D( (xold,x),(yold,y),lw=1.0,label=filename,color="C%d"%index)
               ax.add_line(plline)
            if lineend: lineactive = False
        if dispensedot:
            ax.scatter( x, y, color="C%d"%index )

def drawFEHybridOutline( ax, tile, transform = Transform2D() ):
    import numpy as np
    import matplotlib.pyplot as plt
    from matplotlib.patches import Polygon
    # positions are computed relative to the marker on the tile. those markers
    # are in 'global' coordinates (same system on both sides). that's okay for
    # a rectangle, but for the hybrid we need to turn mirror the c-side in the
    # y-axis.

    # for the position of the hybrid we use the top right corner. we
    # can of course derive this from the tile. so perhaps we should
    # give the 'tile' as argument to the funcion
    #from geometry import FEHYBRID_WIDTH as hybridwidth
    #markerdist = tile.FirstMarker.distance(tile.LastMarker)
    #x0 = -(hybridwidth-markerdist)/2
    #y0 = -0.4
    # the deltas of a polyline
    #deltas = [ (0,0), (hybridwidth,0),(0,-16),(-3,-5),
    #          (0,-4),(-6,-6),(-31,-0),(-3,+7),(0,+24) ]

    # these are the FR hybrid corners relative to corner closest to the first
    # ASIC marker. all y coordinates are zero or negative.
    from geometry import fehybridcorners, fehybridoriginintileframe
    # transform them to the right frame
    T = transform * tile.transform()
    globalpoints = [ T*Point2D( fehybridoriginintileframe[0] + p[0], fehybridoriginintileframe[1] + p[1]) for p in fehybridcorners ]
    # transform them to grpoints frame
    pointpositions = [ [ p.x(), p.y() ] for p in globalpoints ]
    pts = np.array( pointpositions )
    poly = Polygon(pts, closed=True,fill=False,color='green')
    ax.add_patch(poly)


def GTBXHybridOutline( ax, transform = Transform2D() ):
    import numpy as np
    import matplotlib.pyplot as plt
    from matplotlib.patches import Polygon
    # positions are computed relative to the marker on the tile. those markers
    # are in 'global' coordinates (same system on both sides). that's okay for
    # a rectangle, but for the hybrid we need to turn mirror the c-side in the
    # y-axis.

    from geometry import gbtxcorners, gbtxcenter, gbldcenter,Point2D
     # create an array of points in the right frame
    T = transform
    globalpoints   = [ T*Point2D(p[0],p[1]) for p in gbtxcorners]
    pointpositions = [ [ p.x(), p.y() ] for p in globalpoints ]
    poly = Polygon(np.array(pointpositions), closed=True,fill=False,color='green')
    ax.add_patch(poly)
    
    # create also squares for gbtx chip and gblt chip. what a mess.
    positionandsize     = [ (gbtxcenter,16.0), (gbldcenter,4.) ]
    for chip in positionandsize:
        x = chip[0][0]
        y = chip[0][1]
        w = 0.5*chip[1]
        corners = [ (x-w, y-w),(x-w,y+w),(x+w,y+w),(x+w,y-w)]
        globalpoints   = [ T*Point2D(p[0],p[1]) for p in corners]
        pointpositions = [ [ p.x(), p.y() ] for p in globalpoints ]
        poly = Polygon(np.array(pointpositions), closed=True,fill=True,color='green',alpha=0.5,)
        ax.add_patch(poly)

def drawCSideTiles( ax, title, transform ):
    #ax.set_xlim(25,115)
    #ax.set_ylim(45,135)
    from geometry import tilelibrary,csidegbtxtransform
    for tile in ["CLI","CSO"]:
        drawTileOutline(ax,tilelibrary[tile], transform)
        drawFEHybridOutline(ax,tilelibrary[tile], transform)
    GTBXHybridOutline(ax,transform*csidegbtxtransform)
    
def drawNSideTiles( ax, title, transform ):
    #ax.set_xlim(-5,85)
    #ax.set_ylim(45,135)
    from geometry import tilelibrary,nsidegbtxtransform
    for tile in ["NLO","NSI"]:
        drawTileOutline(ax,tilelibrary[tile], transform)
        drawFEHybridOutline(ax,tilelibrary[tile], transform)
    GTBXHybridOutline(ax,transform*nsidegbtxtransform)

def drawOutlines():
    transform = Transform2D()
    import matplotlib.pyplot as plt
    fig, axs = plt.subplots(1,2)
    fig.set_size_inches(8,4)
    fig.subplots_adjust(left=0.05,right=0.95,bottom=0.05,top=0.95,wspace = 0.0,hspace=0.0)
    size = 120
    drawCSideTiles( axs[0], "CSide", transform )
    drawNSideTiles( axs[1], "NSide", transform )
    axs[0].set_aspect('equal')
    axs[1].set_aspect('equal')
    plt.show()

# function to compute the total glue line length in a glue pattern file
def totalLineLength( filename ):
    from math import sqrt
    inputfile = open(filename, "r")
    x = 0.
    y = 0.
    L = 0.
    lineactive = False
    for line in inputfile.readlines():
        if "Line Start" in line:
            lineactive = True
            words = line.split(",")
            x = float(words[1])
            y = float(words[2])
        if ("Line Passing" in line) or ("Line End" in line):
            xold = x
            yold = y
            words = line.split(",")
            x = float(words[1])
            y = float(words[2])
            if lineactive:
                L += sqrt( (x-xold)**2 + (y-yold)**2 )
        if "Line End" in line:
            lineactive = False
    return L

if __name__ == "__main__":
    drawOutlines()
