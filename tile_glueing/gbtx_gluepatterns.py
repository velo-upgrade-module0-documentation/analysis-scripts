# $Id$

from shapely.geometry import Polygon, Point, LineString
from shapely.geometry.polygon import LinearRing
from shapely.affinity import rotate, translate
import matplotlib.pyplot as plt
import numpy as np
import time

#######################################

# Produce a plot

plot = False
plot_outline = False

# Distance from the edge of the GBTx to the beginning of the line (default 5)

distance = 5.

# These are translations in x and y for C side and N side

move_x_C = -8.5
move_y_C = -1.0
z_offset_cside = 45.6 - 0.2

move_x_N = -7.5
move_y_N = 43.0
z_offset_nside = 45.6 - 0.2

#######################################

fig_size = plt.rcParams["figure.figsize"]
 
# print "Current size:", fig_size
 
fig_size[0] = 8
fig_size[1] = 8
plt.rcParams["figure.figsize"] = fig_size




gbtxcenter = [20.5, 22.3]

gbtx = [(0.,0.),
                   (0.,28.7),
                   (7.6,36.3),
                   (30.3,36.3),
                   (43.9,22.7),
                   (43.9,6.5),
                   (37.4,0.),
                   (0.,0.)
                   ]
gbtxmirror = [(43.9,28.7),
                   (43.9-7.6,36.3),
                   (13.6,36.3),
                   (0.,22.7),
                   (0.,6.5),
                   (6.5,0.),
                   (43.9,0.),
                   (43.9,28.7)
                   ]

def getstarpoints(gbtx = gbtx,
					center = gbtxcenter,
					x_translate = 0,
					y_translate = 0,
					plots = False,
          edge = False
					):

    _distance = distance
    default_move = -1.8
    gbtx_width = 41.
    gbtx_height = 44.6
    gbtxcoords = gbtx
    centerpoint = center

    polygon1 = Polygon(gbtxcoords)
    polygon1 = rotate(polygon1, 90, origin=centerpoint)
    polygon1 = translate(polygon1, xoff=default_move-4.6, yoff=default_move)

    polygonline = LineString(gbtxcoords)
    polygonline = rotate(polygonline, 90, origin=centerpoint)
    polygonline = translate(polygonline, xoff=default_move-4.6, yoff=default_move)

    rectangle = Polygon([(0.,0.),
                       (0.,41.),
                       (44.6,41.),
                       (44.6,0.)
                       ])

    rectangle = rotate(rectangle, 90, origin=centerpoint)
    rectangle = translate(rectangle, xoff=default_move, yoff=default_move)

    newcenterpoint = [centerpoint[0]+default_move-4.6,centerpoint[1]+default_move]

    Nx = 3
    Lx = gbtx_width
    Ly = gbtx_height

    Ny = int( Nx * Ly / Lx + 1.0 )
    if Ny < 3 : Ny = 3
    
    x0 = 0.
    y0 = 0.
    dx = Lx/Nx
    dy = Ly/Ny

    # plt.figure(0)
    plt.plot(*polygon1.exterior.xy)
    plt.plot(*rectangle.exterior.xy)

    points = []
    if edge==False:
      points += [ ( x0 + i*dx, y0 ) for i in range(Nx) ]
      points += [ ( x0 + Lx, y0 + i*dy ) for i in range(Ny) ]
      points += [ ( x0 + Lx - i*dx, y0 + Ly ) for i in range(Nx) ]
      points += [ ( x0, y0 + Ly - i*dy ) for i in range(Ny) ]

      lines = []
      for p in points:
      	lines.append(LineString([p, newcenterpoint]))
      
      pointinters = []
      for l in lines:
      	for p in list(l.coords):
      		plt.plot( (p[0],newcenterpoint[0]), (p[1],newcenterpoint[1]), 'b') 
      	
      	interx = polygonline.intersection(l)
      
      	pointinters.append( *list(interx.coords) )
      
      for p in pointinters:
      	plt.plot(p[0], p[1], 'bo') 
      
      lines2 = []
      for p in pointinters:
      	lines2.append(LineString([p, newcenterpoint]))
      
      truepoints = []
      
      for l in lines2:
      	for p in list(l.interpolate(_distance).coords):
      		plt.plot( (p[0],newcenterpoint[0]), (p[1],newcenterpoint[1]), 'go') 
      	truepoints.append( *list(l.interpolate(_distance).coords) )
    
    else:
      truepoints = list(polygon1.exterior.coords)
      x = []
      y = []
      for p in truepoints:
        x.append(float(p[0]))
        y.append(float(p[1]))
      plt.plot(np.array(x), np.array(y), 'go')

    if plots:
    	plt.show()
    plt.clf()
    return truepoints, newcenterpoint

# 73.537,	57.359

def writegbtxpattern( filename,
                        points,
                        centerpoint,
                        move_x = 0,
                        move_y = 0,
                        zoffset = 44.,
                        linespeed = 6,
                        outline = False) :
    move_x += 73.537 - centerpoint[0] + 6.
    move_y += 57.359 - centerpoint[1]

    Zclearance = 1
    headtime = 0.1 #sec
    tailtime = 0.0 #sec
    nodetime = 0.0 #sec
    taillength = 1.0 #mm
    pointtime = 0.1 #sec
    pointtailtime = 0.25 #sec
    outputfile = open(filename, "w")
    outputfile.write(f"Line Speed, {linespeed:3.1f}\n")
    outputfile.write(f"Line Dispense Setup,{headtime:4.2f},{tailtime:5.2f},{nodetime:5.2f},{taillength:5.2f}\n")
    outputfile.write(f"Point Dispense Setup,{pointtime:4.2f},{pointtailtime:5.2f}\n")    
    outputfile.write("Dispense End Setup, 20.0, 5.0, 0.0\n")
    outputfile.write(f"Z Clearance,{Zclearance:3.1f}, 1\n")
    if outline:
            for p in points:
                outputfile.write(f"Dispense Dot,{(p[0]+move_x):8.3f},{(p[1]+move_y):8.3f},{zoffset:8.3f}\n")
    else:
        outputfile.write(f"Dispense Dot,{(centerpoint[0]+move_x):8.3f},{(centerpoint[1]+move_y):8.3f},{zoffset:8.3f}\n")
        for p in points:
            outputfile.write(f"Line Start,  {(p[0]+move_x):8.3f},{(p[1]+move_y):8.3f},{zoffset:8.3f}\n")
            outputfile.write(f"Line End,    {(centerpoint[0]+move_x):8.3f},{(centerpoint[1]+move_y):8.3f},{zoffset:8.3f}\n")
   
    outputfile.write("End Program\n")
    outputfile.close()




writegbtxpattern( filename = "GBTx_STAR_C_side.SRC",
                       points = getstarpoints(plots = False)[0],
                       centerpoint = getstarpoints(plots = plot)[1],
                       move_x = move_x_C,
                       move_y = move_y_C,
                       zoffset = z_offset_cside,
                       linespeed = 6,
                       )


writegbtxpattern( filename = "GBTx_STAR_N_side.SRC",
                       points = getstarpoints(plots = False)[0],
                       centerpoint = getstarpoints(plots = plot)[1],
                       move_x = move_x_N,
                       move_y = move_y_N,
                       zoffset = z_offset_nside,
                       linespeed = 6,
                       )

writegbtxpattern( filename = "outline_GBTx_STAR_C_side.SRC",
                       points = getstarpoints(plots = False, edge = True)[0],
                       centerpoint = getstarpoints(plots = plot_outline, edge = True)[1],
                       move_x = move_x_C,
                       move_y = move_y_C,
                       zoffset = z_offset_cside,
                       linespeed = 6,
                       outline = True
                       )


writegbtxpattern( filename = "outline_GBTx_STAR_N_side.SRC",
                       points = getstarpoints(plots = False, edge = True)[0],
                       centerpoint = getstarpoints(plots = plot_outline, edge = True)[1],
                       move_x = move_x_N,
                       move_y = move_y_N,
                       zoffset = z_offset_nside,
                       linespeed = 6,
                       outline = True
                       )

print(time.asctime())
