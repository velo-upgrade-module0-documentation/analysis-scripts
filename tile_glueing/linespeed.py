import numpy as np
from scipy import optimize

def test_func(x, a, t):
    return a * np.exp(-x/t)

input_x0 = []
input_y0 = []

values = input('Number of measurement pairs to input: ')

for i in range(values):
	input_x0.append( input('Add a time measurement: ') )
	print input_x0
	input_y0.append( input('Add a mass measurement: ') )
	print input_y0

# Some values as an example

# input_x0 = [6.75, 9.00, 11.00]
# input_y0 = [0.0736, 0.0698, 0.0653]

# C side N010 as an example

# input_x0 = [15.00, 18.00, 21.00, 25.00]
# input_y0 = [0.0987, 0.0904, 0.0842, 0.0783]

# Hardcoded values for the reference curve and reference glueing start times

x0_data = [5.00, 8.00, 18.42, 23.83, 26.66, 29.00, 31.25]
y0_data = [0.1030, 0.0951, 0.0703, 0.0608, 0.0578, 0.0547, 0.0499]

x_glueing_1 = [10.00] 
y_glueing_1 = [0.090]

x_glueing_2 = [13.00]
y_glueing_2 = [0.083]



params, params_covariance = optimize.curve_fit(test_func, x0_data, y0_data, p0=[2, 2])

print(params)

params2, params_covariance2 = optimize.curve_fit(test_func, input_x0, input_y0, p0=[2, 2])

print(params2)

import matplotlib.pyplot as plt
plt.figure(figsize=(11, 9))

plt.plot(np.arange(1,50), test_func(np.arange(1,50), params[0], params[1]), 'c', 
         label=r'Reference curve with $a*e^{-\lambda/\tau}$'+str(params))

plt.scatter(x_glueing_1, y_glueing_1, c='b', label='Reference 1st tile: start at 10 min after mixing with linespeed 9.0')
plt.scatter(x_glueing_2, y_glueing_2, c='c', label='Reference 2nd tile: start at 13 min after mixing with linespeed 8.3')

plt.plot(np.arange(1,50), test_func(np.arange(1,50), params2[0], params2[1]), 'r', 
         label=r'Our curve with $a*e^{-\lambda/\tau}$'+str(params2))

plt.scatter(input_x0, input_y0, c='r', label='Measured points')

extrapolate_x = [ input_x0[-1]+3, input_x0[-1]+6 ] 
extrapolate_y = [ test_func( input_x0[-1]+3, params2[0], params2[1]), test_func( input_x0[-1]+6, params2[0], params2[1] ) ]

print extrapolate_x
print extrapolate_y


plt.scatter(extrapolate_x[0], extrapolate_y[0], c='g', 
            label='To glue the 1st tile at '+str(extrapolate_x[0])+' minutes with linespeed '+str(round(extrapolate_y[0]*100, 2)))
plt.scatter(extrapolate_x[1], extrapolate_y[1], c='m', 
            label='To glue the 2nd tile at '+str(extrapolate_x[1])+' minutes with linespeed '+str(round(extrapolate_y[1]*100, 2)))

plt.plot([0, 20, 50], [0.06, 0.06, 0.06], label='Glue is too thick below this point')

plt.legend(loc='best')
plt.xlim(0,50)
plt.ylim(0,0.15)
plt.xticks(np.arange(0, 50, step=5))

plt.xlabel('Time after mixing, [minutes]')
plt.ylabel('Glue deposited, [grams/60 seconds]')
plt.suptitle('Glue flow vs time')

plt.savefig('GlueFlow.png', dpi=150)
plt.show()