# $Id$
from __future__ import print_function
import time
import numpy

distance = 0.15 # distance between the needle tip and the tile
line_speed = 20

### put in here the MEASURED values

### to determine LEFT and RIGHT look from the bond side
###

### TODO print out these lines in the SRC
### meanwhile you will have to check them manually
### This only produces proper files if the input is more or less a plane

# To align the pattern, you can take measurements at the tile
# corners. The function will automatically look for the nearest
# expected corners, then recompute the coordinate system. You need to
# specify at least two points.
# example: csideCornerMeasurements = [ ( X1, Y1 ), (X2, Y2) ] etc.
csideCornerMeasurements = []
nsideCornerMeasurements = []
#csideCornerMeasurements = [ (103.33, 91.24), (74.22, 120.86), (42.03,51.36), (71.78,80.92) ]
#csideCornerMeasurements = [ (74.22, 120.86), (42.03,51.36), (71.78,80.92) ]
#nsideCornerMeasurements = [(45.66,84.09),(24.47,124.37),(13.77,111.98),(43.26,54.20),
#                            (30.87,64.98),(71.10,86.23),(58.55,97.10),(56.41,96.47)]
#csideCornerMeasurements = [(42.17,51.01), (74.33,121.37),(62.73,109.58), (104.33,91.28)]

### Do check the output before glueing
robot_zoffset_CSO_bond_right = 45.0 ### line 008
robot_zoffset_CSO_beam_right = 45.0 ### line 046
robot_zoffset_CSO_bond_left  = 45.0 ### line 126
robot_zoffset_CSO_beam_left  = 45.0 ### line 140

###
robot_zoffset_CLI_bond_right = 45.0 ### line 008
robot_zoffset_CLI_beam_right = 45.0 ### line 040
robot_zoffset_CLI_bond_left  = 45.0 ### line 102
robot_zoffset_CLI_beam_left  = 45.0 ### line 110

###
robot_zoffset_NLO_bond_right = 45.0 ### line 008
robot_zoffset_NLO_beam_right = 45.0 ### line 046
robot_zoffset_NLO_bond_left  = 45.0 ### line 126
robot_zoffset_NLO_beam_left  = 45.0 ### line 140

###
robot_zoffset_NSI_bond_right = 45.0 ### line 008
robot_zoffset_NSI_beam_right = 45.0 ### line 040
robot_zoffset_NSI_bond_left  = 45.0 ### line 114
robot_zoffset_NSI_beam_left  = 45.0 ### line 128

###
dist_to_edgeX  = 1.9 # minimal distance of glue to ASIC edge in mm in X direction (width)  (default 1.25)
dist_to_edgeY  = 1.9 # minimal distance of glue to ASIC edge in mm in Y direction (height) (default 1.50)

#Print the alignment in power shell
print()
print ("Line speed: "+"{:.2f}".format(line_speed))
print()
print ("CSO_008: "+"{:.2f}".format(robot_zoffset_CSO_bond_right))
print ("CSO_046: "+"{:.2f}".format(robot_zoffset_CSO_beam_right))
print ("CSO_126: "+"{:.2f}".format(robot_zoffset_CSO_bond_left))
print ("CSO_140: "+"{:.2f}".format(robot_zoffset_CSO_beam_left))
print()
print ("CLI_008: "+"{:.2f}".format(robot_zoffset_CLI_bond_right))
print ("CLI_040: "+"{:.2f}".format(robot_zoffset_CLI_beam_right))
print ("CLI_102: "+"{:.2f}".format(robot_zoffset_CLI_bond_left))
print ("CLI_110: "+"{:.2f}".format(robot_zoffset_CLI_beam_left))
print()
print ("NLO_008: "+"{:.2f}".format(robot_zoffset_NLO_bond_right))
print ("NLO_046: "+"{:.2f}".format(robot_zoffset_NLO_beam_right))
print ("NLO_126: "+"{:.2f}".format(robot_zoffset_NLO_bond_left))
print ("NLO_140: "+"{:.2f}".format(robot_zoffset_NLO_beam_left))
print()
print ("NSI_008: "+"{:.2f}".format(robot_zoffset_NSI_bond_right))
print ("NSI_040: "+"{:.2f}".format(robot_zoffset_NSI_beam_right))
print ("NSI_102: "+"{:.2f}".format(robot_zoffset_NSI_bond_left))
print ("NSI_110: "+"{:.2f}".format(robot_zoffset_NSI_beam_left))
print()
###
### Below this line you should not in principle have to modify anything
###


Zclearance  = 5 # mm upward movement after glue deposit

robot_xoffset_cside = 53.107     # x position of LHCb origin in frame of robot
robot_yoffset_cside = 92.602     # y position of LHCb origin in frame of robot
robot_angle_cside   = -0.00146  # rotation angle

robot_xoffset_nside = 30.6   # x position of LHCb origin in frame of robot
robot_yoffset_nside = 72.5   # y position of LHCb origin in frame of robot
robot_angle_nside   = -0.0688  # rotation angle
#############

robot_zoffset_hybrids = 42   # z position of surface in glue robot
robot_xoffset_hybrids = -10.7   # x position of LHCb origin in frame of robot
robot_yoffset_hybrids = 95.9   # y position of LHCb origin in frame of robot

starpatternlinesNx = 6
starpatterncurveparameterX = 0.8 # fraction by which central line is shorter on X side in star pattern
starpatterncurveparameterY = 0.9 # fraction by which central line is shorter on Y side in star pattern

# class to represent a point in 2D space. internally parametrized by
# numpy.matrix. I couldn't find a good python alternative
from geometry import Point2D, Transform2D

# This function write to file the linepattern for one asic. Coordinates
# are defined relative to its lower left corner. Use the transform to
# get the coordinates in the frame of the glue machine.

from geometry import asic_interval, asic_width, asic_height, tile_width, tile_height
def linepattern( outputfile,
                 transform,
                 overhang,
                 zoffset,
                 nlines = 12,
                 widthscale  = 1.0,
                 heightscale = 1.0 ):
    # compute the space between the lines
    linespacing = widthscale*(asic_width-2*dist_to_edgeX)/(nlines-1)
    linelength  = heightscale*(asic_height - overhang - 2*dist_to_edgeY)
    xoffset     = -0.5*(nlines-1)*linespacing
    yoffset     = heightscale*(dist_to_edgeY - 0.5*asic_height)
    for i in range(0,nlines):
        x1 = Point2D( xoffset + i*linespacing, yoffset )
        x2 = Point2D( xoffset + i*linespacing, yoffset+linelength)
        x1p = transform * x1
        x2p = transform * x2
        outputfile.write("Line Start,    %8.3f,%8.3f,%8.3f\n" % (x1p.x(),x1p.y(),zoffset))
        #outputfile.write("Line Passing,  %8.3f,%8.3f,%8.3f\n" % (x1p.x(),x1p.y(),zoffset))
        outputfile.write("Line End,      %8.3f,%8.3f,%8.3f\n" % (x2p.x(),x2p.y(),zoffset))

# same as above, but now for a snake pattern
def snakepattern( outputfile,
                 transform,
                 overhang,
                 zoffset,
                 nlines=12,
                 widthscale  = 1.0,
                 heightscale = 1.0 ):
    linespacing = widthscale*(asic_width-2*dist_to_edgeX)/(nlines-1)
    linelength  = heightscale*(asic_height - overhang - 2*dist_to_edgeY)
    xoffset     = -0.5*(nlines-1)*linespacing
    yoffset     = heightscale*(dist_to_edgeY - 0.5*asic_height)

    # first make a list of all the points on the lines. only then write them to the file.
    localpoints = []
    direction = -1
    yoffset += linelength
    for i in range(0,nlines):
        localpoints.append( (xoffset,yoffset) )
        yoffset += direction * linelength
        localpoints.append( (xoffset,yoffset) )
        xoffset += linespacing
        direction *= -1
    # transform the localpoints into global coordinates
    globalpoints = []
    for p in localpoints:
        globalpoints.append( transform * Point2D( x=p[0], y=p[1] ) )
    # now export the lines
    index=0
    for p in globalpoints:
        coords = (p.x(),p.y(),zoffset )
        if index==0:
            outputfile.write("Line Start,  %8.3f,%8.3f,%8.3f\n" % coords)
        if index<len(globalpoints)-1:
            outputfile.write("Line Passing,%8.3f,%8.3f,%8.3f\n" % coords)
        else:
            outputfile.write("Line End,    %8.3f,%8.3f,%8.3f\n" % coords)
        index += 1
    #outputfile.write("Dispenser On/Off,0\n")

# same as above, but now for a Daniel's pattern
def danielpattern( outputfile,
                   transform,
                   overhang,
                   zoffset ):
    return snakepattern( outputfile=outputfile,transform=transform,overhang=overhang,zoffset=zoffset,
                         nlines = 12, widthscale=0.5, heightscale=0.5)

# same idea but now using the brush function
def brushpattern( outputfile,
                   transform,
                   overhang,
                   zoffset,
                   widthscale=0.5, heightscale=0.5 ):
    Lx = widthscale*(asic_width-2*dist_to_edgeX)
    Ly = heightscale*(asic_height-overhang-2*dist_to_edgeY)
    x0 = -0.5*Lx
    y0 = -heightscale*(0.5*asic_height - dist_to_edgeY)
    p1 = transform * Point2D(x0,y0)
    p2 = transform * Point2D(x0+Lx,y0+Ly)
    brushwidth = 0.5 # mm
    outputfile.write("Brush Area,%f,0,1,0\n" % brushwidth)
    outputfile.write("Line Start,  %8.3f,%8.3f,%8.3f\n" % (p1.x(),p1.y(),zoffset) )
    outputfile.write("Line End,  %8.3f,%8.3f,%8.3f\n" % (p2.x(),p2.y(),zoffset) )

def tilezoffsetfunction( thetile, fromAsicToTile, positionInAsicFrame ):
    # this function delegates the z position computation to the tile
    # and subtracts the glue thickness. we just need to deal with the
    # proper transformation.
    positionInTileFrame = fromAsicToTile * positionInAsicFrame
    return thetile.z( localX = positionInTileFrame.x(), localY = positionInTileFrame.y() ) - distance
    
def starpattern( outputfile,
                 transform,
                 overhang,
                 zoffsetfunction,
                 Nx = starpatternlinesNx,
                 curveparameterX = starpatterncurveparameterX,
                 curveparameterY = starpatterncurveparameterY ):

    # lines coming from a common center. if we draw them from the edge
    # to the center, then we do not need to worry about glue dripping.
    Lx = asic_width-2*dist_to_edgeX
    Ly = asic_height-overhang-2*dist_to_edgeY
    #Ny = int( Nx * Ly / Lx + 0.5 )
    Ny = int( Nx * Ly / Lx )
    if Ny < 3 : Ny = 3
    x0 = -0.5*Lx
    y0 = -0.5*asic_height + dist_to_edgeY
    dx = Lx/Nx
    dy = Ly/Ny

    #print "Nx, Ny: ", Nx, Ny, dx, dy

    # this was the original code
    # points = []
    #points += [ Point2D( x0 + i*dx, y0 ) for i in range(Nx) ]
    #points += [ Point2D( x0 + Lx, y0 + i*dy ) for i in range(Ny) ]
    #points += [ Point2D( x0 + Lx - i*dx, y0 + Ly ) for i in range(Nx) ]
    #points += [ Point2D( x0, y0 + Ly - i*dy ) for i in range(Ny) ]

    # with the non-square pattern it has become a bit more cumbersome
    # we parametrize x (or y) as a function of y (or x) using the curve parameter
    # dx(dy) = a + b*abs(dy)
    points1 = []
    points2 = []
    points3 = []
    points4 = []
    # alternative: keep one set of coordinates the same, only move the
    # other coordinate. this will make the lines in the corner move closer.
    for i in range(Nx):
        eta = curveparameterY
        a =  eta*Ly/2
        b = (1-eta)*Ly/Lx
        d_i  = (2.*i/Nx-1) # runs between -1 and 1
        dx_i = d_i*Lx/2
        dy_i = a + (Ly/2-a)*abs(d_i)
        points1.append( Point2D( x0 + Lx/2 + dx_i, y0 + Ly/2 - dy_i ) )
        points3.append( Point2D( x0 + Lx/2 - dx_i, y0 + Ly/2 + dy_i ) )
    for i in range(Ny):
        eta = curveparameterX
        a =  eta*Lx/2
        b = (1-eta)*Lx/Ly
        d_i  = (2.*i/Ny-1)
        dy_i = d_i*Ly/2
        dx_i = a + (Lx/2-a)*abs(d_i)
        points2.append( Point2D( x0 + Lx/2 + dx_i , y0 + Ly/2 + dy_i ) )
        points4.append( Point2D( x0 + Lx/2 - dx_i , y0 + Ly/2 - dy_i ) )

    points = points1 + points2 + points3 + points4
    midpoint =  Point2D( 0, y0 + 0.5*Ly )
    zmidpoint = zoffsetfunction( midpoint )
    x2 = transform * midpoint

    if starpattern.counter == 0:
        outputfile.write("Dispense Dot,  %8.3f,%8.3f,%8.3f\n" % (x2.x(),x2.y(),zmidpoint))
    for p in points:
        x1 = transform * p
        outputfile.write("Line Start,    %8.3f,%8.3f,%8.3f\n" % (x1.x(),x1.y(),zoffsetfunction( p ) ))
        outputfile.write("Line End,      %8.3f,%8.3f,%8.3f\n" % (x2.x(),x2.y(),zmidpoint))
            # outputfile.write("Dispense Dot,  %8.3f,%8.3f,%8.3f\n" % (x2.x(),x2.y(),zmidpoint))

# This function creates a 'SRC' file for a tile with three asics. It
# takes as arguments the filename, the transformation from LHCB
# coordinates to glue robot coordinates and the function that makes
# the patterns. 'linelengths' is a vector that specifies the length of
# the lines for the three different asics.
def writethreeasicpattern( filename,
                           tileinfo,
                           robottransform,
                           patternfunction,
                           linespeed = line_speed ) :
    
    headtime = 0.1 #sec
    tailtime = 0.0 #sec
    nodetime = 0.0 #sec
    taillength = 1.0 #mm
    pointtime = 0.1 #sec
    pointtailtime = 0.25 #sec
    outputfile = open(filename, "w")
    outputfile.write("Program Name,%s\n"%filename)
    outputfile.write("Line Speed,%3.1f\n" % linespeed)
    outputfile.write("Line Dispense Setup,%4.2f,%5.2f,%5.2f,%5.2f\n" % (headtime,tailtime,nodetime,taillength))
    outputfile.write("Point Dispense Setup,%4.2f,%5.2f\n" % (pointtime,pointtailtime))
    outputfile.write("Dispense End Setup,20.0,5.0,0.0\n")
    outputfile.write("Z Clearance,%3.1f,1\n" % Zclearance)

    # the transform is the transform of the leftmost asic. for the others, we need to add an offset
    # unfortunately the 
    tiletransform = robottransform*tile.transform()
    starpattern.counter = 0
    for iasic in range(0,3) :
        outputfile.write("Label,%1d\n" % (iasic+1) )
        asictransform = Transform2D()
        asictransform.setTranslation( dx = iasic * asic_interval + 0.5*asic_width, dy = 0.5*asic_height)
        patternfunction( outputfile = outputfile,
                         transform = tiletransform*asictransform,
                         overhang = tile.Overhangs[iasic],
                         zoffsetfunction = lambda x : tilezoffsetfunction(tile,asictransform,x))
        starpattern.counter += 1
    outputfile.write("End Program\n")
    outputfile.close()
    from visualization import totalLineLength
    print ("total line length: " + filename, totalLineLength( filename ) )

# This function creates a 'SRC' file for a single FE hybrid
def writehybridpattern( filename, transform, linespeed, move_x, move_y ) :
    outputfile = open(filename, "w")
    outputfile.write("Line Speed,%f\n" % linespeed)
    outputfile.write("Z Clearance, 2, 1\n")
    zoffset     = robot_zoffset_hybrids
    # let's start with three lines. coordinates are defined relative to a
    # marker on the tile, because that's where we get the transform from
    x1p = transform * Point2D( +1.0, -2.0-2. )
    x2p = transform * Point2D( 42.0, -2.0-2. )
    outputfile.write("Line Start,    %8.3f,%8.3f,%8.3f\n" % (x1p.x()+ move_x,x1p.y()+ move_y,zoffset))
    outputfile.write("Line End,      %8.3f,%8.3f,%8.3f\n" % (x2p.x()+ move_x,x2p.y()+ move_y,zoffset))

    x1p = transform * Point2D( +1.0, -29.0+(26.95-5.65) )
    x2p = transform * Point2D( 42.0, -29.0+(26.95-5.65) )
    outputfile.write("Line Start,    %8.3f,%8.3f,%8.3f\n" % (x1p.x()+ move_x,x1p.y()+ move_y,zoffset))
    outputfile.write("Line End,      %8.3f,%8.3f,%8.3f\n" % (x2p.x()+ move_x,x2p.y()+ move_y,zoffset))

    x1p = transform * Point2D( +1.0, -29.0+(26.95-20.07) )
    x2p = transform * Point2D( 39.0, -29.0+(26.95-20.07) )
    outputfile.write("Line Start,    %8.3f,%8.3f,%8.3f\n" % (x1p.x()+ move_x,x1p.y()+ move_y,zoffset))
    outputfile.write("Line End,      %8.3f,%8.3f,%8.3f\n" % (x2p.x()+ move_x,x2p.y()+ move_y,zoffset))

    x1p = transform * Point2D( +5.0, -29.0+1. )
    x2p = transform * Point2D( 36.0, -29.0+1. )
    outputfile.write("Line Start,    %8.3f,%8.3f,%8.3f\n" % (x1p.x()+ move_x,x1p.y()+ move_y,zoffset))
    outputfile.write("Line End,      %8.3f,%8.3f,%8.3f\n" % (x2p.x()+ move_x,x2p.y()+ move_y,zoffset))

    outputfile.write("End Program\n")
    outputfile.close()

# this is for debugging positions: it just makes the robot move along the sides of the tile
def writetileoutline( filename,
                      tileinfo,
                      robottransform,
                      use_dist_to_edge = False,
                      linespeed = 10 ) :
    outputfile = open(filename, "w")
    outputfile.write("Line Speed,%f\n" % linespeed)
    x0 = 0
    y0 = 0
    x1 = x0 + tile_width
    y1 = y0 + tile_height
    if use_dist_to_edge:
        x0 += dist_to_edgeX
        x1 -= dist_to_edgeX
        y0 += dist_to_edgeY
        y1 -= dist_to_edgeY

    # this is counterintuitive because we look from the rear. we keep the order the way people got used to.
    localpoints = [ (x0,y0,tileinfo.ZOffset_bond_right),(x0,y1,tileinfo.ZOffset_beam_right),
                    (x1,y0,tileinfo.ZOffset_bond_left),(x1,y1,tileinfo.ZOffset_beam_left) ]
    for l in localpoints:
        p = transform * Point2D( x=l[0], y=l[1] )
        outputfile.write("Dispense Dot, %8.3f,%8.3f,%8.3f\n" % (p.x(),p.y(),l[2]-distance) )
    outputfile.write("End Program\n")
    outputfile.close()

# class to keep the coordinates of a tile. from the position of two
# markers it can compute the transform to go from coordinates in the
# tile, to coordinates in the LHCB frame.
from geometry import tilelibrary, TileInfo

# temporarily add the z information to the TileInfo class. need to find a nicer way to do this:
def z(self, localX, localY ):
    # compute the robot coordinates of the tile from localX and
    # localY. for now we do bi-linear interpolation. in the near
    # future I rather fit a plane: if we store the plane slopes
    # then it will become enough to measure just one z-position on
    # the tile.
    wx = (localX-dist_to_edgeX) / (tile_width-2*dist_to_edgeX)
    wy = (localY-dist_to_edgeY) / (tile_height-2*dist_to_edgeY)
    return self.ZOffset_bond_right*(1-wx)*(1-wy) + self.ZOffset_bond_left*wx*(1-wy) + self.ZOffset_beam_right*(1-wx)*wy + self.ZOffset_beam_left * wx*wy
TileInfo.z = z

tilelibrary["CLI"].ZOffset_bond_left = robot_zoffset_CLI_bond_left
tilelibrary["CLI"].ZOffset_bond_right = robot_zoffset_CLI_bond_right
tilelibrary["CLI"].ZOffset_beam_left = robot_zoffset_CLI_beam_left
tilelibrary["CLI"].ZOffset_beam_right = robot_zoffset_CLI_beam_right 

tilelibrary["CSO"].ZOffset_bond_left = robot_zoffset_CSO_bond_left
tilelibrary["CSO"].ZOffset_bond_right = robot_zoffset_CSO_bond_right
tilelibrary["CSO"].ZOffset_beam_left = robot_zoffset_CSO_beam_left
tilelibrary["CSO"].ZOffset_beam_right = robot_zoffset_CSO_beam_right

tilelibrary["NLO"].ZOffset_bond_left = robot_zoffset_NLO_bond_left
tilelibrary["NLO"].ZOffset_bond_right = robot_zoffset_NLO_bond_right
tilelibrary["NLO"].ZOffset_beam_left = robot_zoffset_NLO_beam_left
tilelibrary["NLO"].ZOffset_beam_right = robot_zoffset_NLO_beam_right

tilelibrary["NSI"].ZOffset_bond_left = robot_zoffset_NSI_bond_left
tilelibrary["NSI"].ZOffset_bond_right = robot_zoffset_NSI_bond_right
tilelibrary["NSI"].ZOffset_beam_left = robot_zoffset_NSI_beam_left
tilelibrary["NSI"].ZOffset_beam_right = robot_zoffset_NSI_beam_right 


# transformations to get from the frame in which the marker positions are defined to the frame of the glue robot
csideRobotTransform = Transform2D( name = "CSideRobotTransform" )
csideRobotTransform.setParameters( robot_xoffset_cside,robot_yoffset_cside,robot_angle_cside )
nsideRobotTransform = Transform2D( name = "NSideRobotTransform" )
nsideRobotTransform.setParameters( robot_xoffset_nside,robot_yoffset_nside,robot_angle_nside )

# calibraterobottransforms
def calibrateRobotTransform( tileinfos, cornermeasurements, robottransform ):
    print("Before calibration: ")
    robottransform.dump()
    # take the measurements of the C-side points
    refpointslhcb = []
    # find the closest corners of the tiles
    N = len(cornermeasurements)
    for p in cornermeasurements:
        # loop over the tiles
        closestCornerRobotframe = None
        closestCornerLHCbframe  = None
        closestDistance2 = 0
        for tile in tileinfos:
            # get the four corners, in the LHCb frame ? or directly in the robot frame?
            cornersLHCbFrame = [ tile.transform()*c for c in tile.cornersTileFrame() ]
            for clhcb in cornersLHCbFrame:
                c = robottransform*clhcb
                dx = p[0] - c.x() ;
                dy = p[1] - c.y() ;
                d2 = dx*dx+dy*dy
                if (not closestCornerRobotframe) or (d2<closestDistance2):
                    closestCornerLHCbframe  = clhcb
                    closestCornerRobotframe = c
                    closestDistance2 = d2
        print("Closest corner found: %s %2.3f " % (closestCornerRobotframe.name, closestDistance2) )
        refpointslhcb.append( closestCornerLHCbframe )
        
    # do a chi2 fit? or just use the first two points?
    # let's just first plot the same dx and dy that Aleksandra would compute
    for i in range(0,2):
        # let's now compute dx, dy and dphi and check how the residuals become. we first just use the first two transforms
        refpoints = [ robottransform * c for c in refpointslhcb]
        # let's do a proper chi2 fit. we need to know the derivatives to the
        # delta transform. for that we actually do not need the coordinates in
        # the lhcb frame, but only in the robot frame.
        #    x' = T + R* x
        # for given phi, R is given by
        #  R(0,0) = R(1,1) cos(phi)
        #  R(0,1) = -R(1,0) = sin(phi)
        dChi2dPar = numpy.array( [ [0.], [0.], [0.] ] )
        d2Chi2dPar2 = numpy.array( [ [0.,0.,0.],[0.,0.,0.],[0.,0.,0.] ] )
        for i in range(0,N):
            # now fill the derivative matrix dXdpar. remember that current R is just the identity matrix:
            p = refpoints[i] ;
            H = numpy.array( [ [1, 0], [ 0, 1 ], [ p.y() , -p.x() ] ] )
            resx = cornermeasurements[i][0] - p.x()
            resy = cornermeasurements[i][1] - p.y()
            print("residual: %s %f %f" % (p.name,resx,resy) )
            res = numpy.array( [ [resx], [resy] ] )
            dChi2dPar += H.dot(res)
            d2Chi2dPar2 += H.dot(numpy.transpose(H))
        # now solve
        deltapar = numpy.linalg.solve( d2Chi2dPar2, dChi2dPar )
        print("delta: ", deltapar)
                
        # compute a new transform and look again at the residuals
        delta = Transform2D(name = "delta")
        delta.setTranslation( deltapar[0][0], deltapar[1][0] )
        delta.setRotation( deltapar[2][0] )
        newrobottransform = delta * robottransform
        newrobottransform.name = robottransform.name
        robottransform = newrobottransform
    return robottransform

# main routine. This creates 4 pattern files.

if __name__ == "__main__":
    from geometry import ntileinfos,ctileinfos
    if len( csideCornerMeasurements ) >=2:
        csideRobotTransform = calibrateRobotTransform( ctileinfos, csideCornerMeasurements, csideRobotTransform )
    if len( nsideCornerMeasurements ) >=2:
        nsideRobotTransform = calibrateRobotTransform( ntileinfos, nsideCornerMeasurements, nsideRobotTransform )
    csideRobotTransform.dump()
    nsideRobotTransform.dump()

    from geometry import tileinfos
    for tile in tileinfos:
        robottransform = csideRobotTransform
        if tile.isNSide() : robottransform = nsideRobotTransform
        transform = robottransform * tile.transform()
        # writethreeasicpattern( filename = tile.Name + "_SNAKE.SRC",
        #                        transform = transform,
        #                        patternfunction = snakepattern,
        #                        overhangs = tile.Overhangs,
        #                        zoffset = tile.ZOffset )
        # writethreeasicpattern( filename = tile.Name + "_LINE.SRC",
        #                        transform = transform,
        #                        patternfunction = linepattern,
        #                        overhangs = tile.Overhangs,
        #                         zoffset = tile.ZOffset )
        # writethreeasicpattern( filename = tile.Name + "_SOLID.SRC",
        #                        transform = transform,
        #                        patternfunction = danielpattern,
        #                        overhangs = tile.Overhangs,
        #                         zoffset = tile.ZOffset )
        # writethreeasicpattern( filename = tile.Name + "_BRUSH.SRC",
        #                        transform = transform,
        #                        patternfunction = brushpattern,
        #                        overhangs = tile.Overhangs,
        #                         zoffset = tile.ZOffset )
        writethreeasicpattern( filename = tile.Name + "_STAR.SRC",
                               tileinfo = tile,
                               robottransform = robottransform,
                               patternfunction = starpattern)
        writetileoutline( filename = tile.Name + "_TILEOUTLINE.SRC",
                          tileinfo = tile,
                          robottransform = robottransform )
        writetileoutline( filename = tile.Name + "_PATTERNOUTLINE.SRC",
                          tileinfo = tile,
                          robottransform = robottransform,
                          use_dist_to_edge=True)
        # writehybridpattern( filename = tile.Name + "_FEHYBRID.SRC",
        #                         transform = transform, linespeed = 6, move_x = 0, move_y = 0)
    print(time.asctime())
    
def test():
    import matplotlib.pyplot as plt
    from visualization import drawCSideTiles
    fig, axs = plt.subplots(1,1)
    fig.set_size_inches(8,8)
    drawCSideTiles(axs,"c-side", transform = csiderobottransform)
    plt.show()

def testcside( pattern = "STAR", directory="." ):
    import matplotlib.pyplot as plt
    from visualization import drawPattern, drawCSideTiles
    fig, ax = plt.subplots(1,1)
    fig.set_size_inches(8,8)
    drawPattern(ax, directory + "/CLI_%s.SRC" % pattern)
    drawPattern(ax, directory + "/CSO_%s.SRC" % pattern)
    drawPattern(ax, directory + "/CLI_FEHYBRID.SRC")
    drawPattern(ax, directory + "/CSO_FEHYBRID.SRC")
    drawCSideTiles(ax,pattern, transform = csiderobottransform)
    plt.show()

def testnside( pattern = "STAR", directory="." ):
    import matplotlib.pyplot as plt
    fig, ax = plt.subplots(1,1)
    fig.set_size_inches(8,8)
    from visualization import drawPattern, drawNSideTiles
    drawPattern(ax,directory + "/NSI_%s.SRC" % pattern)
    drawPattern(ax,directory + "/NLO_%s.SRC" % pattern)
    drawNSideTiles(ax,pattern, transform = nsiderobottransform)
    plt.show()
    #grdrawPattern(win,"NSI_FEHYBRID.SRC")
    #grdrawPattern(win,"NLO_FEHYBRID.SRC")

def drawpatterns( pattern = "STAR", directory="." ):
    import matplotlib.pyplot as plt
    fig, axs = plt.subplots(1,2)
    fig.set_size_inches(12,6)
    fig.subplots_adjust(left=0.05,right=0.98,bottom=0.05,top=0.95,wspace = 0.2,hspace=0.2)
    from visualization import drawPattern, drawCSideTiles, drawNSideTiles
    drawCSideTiles(axs[0],pattern, transform = csideRobotTransform)
    drawPattern(axs[0], directory + "/CLI_%s.SRC" % pattern)
    drawPattern(axs[0], directory + "/CSO_%s.SRC" % pattern)
    drawNSideTiles(axs[1],pattern, transform = nsideRobotTransform)
    drawPattern(axs[1],directory + "/NSI_%s.SRC" % pattern)
    drawPattern(axs[1],directory + "/NLO_%s.SRC" % pattern)
    axs[0].set_aspect('equal')
    axs[1].set_aspect('equal')
    axs[0].set_title("c-side %s" % pattern)
    axs[1].set_title("n-side %s" % pattern)
    fig.savefig(directory + '/patterns_%s.pdf' % pattern)
    plt.show()
