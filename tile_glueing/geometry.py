# class to represent a point in 2D space. internally parametrized by
# numpy.matrix. I couldn't find a good python alternative
import numpy
class Point2D:
    __slots__ = [ "m", "name" ]
    def __init__(self, x=0., y=0., name="" ):
        self.m = numpy.array( [[x],[y]] )
        self.name = name
    def x(self): return self.m[0,0]
    def y(self): return self.m[1,0]
    def distance(self, rhs ):
        from math import sqrt
        dx = rhs.x() - self.x()
        dy = rhs.y() - self.y()
        return sqrt(dx*dx+dy*dy)
    def __str__(self):
        return "Point2D: x=%f, y=%f" %(self.x(),self.y())

# class to represent a coordinate transformation (translation+rotation) in 2D space.
class Transform2D:
    __slots__ = [ "translation","rotation","name" ]
    def __init__(self, translation = [[0.],[0.]], rotation = [[1.,0.],[0.,1.]],
                     name = "Noname" ) :
        import copy
        self.translation = copy.deepcopy(numpy.array(translation))
        self.rotation = copy.deepcopy(numpy.array(rotation))
        self.name = name
    def setRotation( self, angle ):
        from math import cos,sin
        self.rotation[0,0] = cos(angle)
        self.rotation[0,1] = sin(angle)
        self.rotation[1,0] = -self.rotation[0,1]
        self.rotation[1,1] = self.rotation[0,0]
    def setTranslation( self, dx, dy ):
        self.translation[0,0] = dx
        self.translation[1,0] = dy
    def setParameters( self, dx, dy, angle):
        self.setRotation(angle)
        self.setTranslation(dx,dy)
    def __mul__(self,other):
        if( isinstance(other,Point2D) ) :
            rc = Point2D( name = other.name)
            rc.m = self.translation + self.rotation.dot(other.m)
            return rc
        elif( isinstance(other,Transform2D) ) :
            product = Transform2D(name = "%s * %s" % (self.name,other.name))
            product.translation = self.translation + self.rotation.dot(other.translation )
            product.rotation = self.rotation.dot(other.rotation)
            return product
    def inverse(self):
        rc = Transform2D(name = "inverse of %s" % self.name )
        det = self.rotation[0,0] * self.rotation[1,1] - self.rotation[1,0] * self.rotation[0,1]
        rc.rotation[0,0] = self.rotation[1,1]/det
        rc.rotation[1,1] = self.rotation[0,0]/det
        rc.rotation[0,1] = -self.rotation[0,1]/det
        rc.rotation[1,0] = -self.rotation[1,0]/det
        rc.translation = -1 * rc.rotation.dot(self.translation)
        return rc
    def dump(self):
        from math import atan2
        angle = atan2( self.rotation[0,1], self.rotation[0,0])
        print("Transform3D: %s dx=%6.3f dy=%6.3f dphi=%6.5f" % (self.name, self.translation[0], self.translation[1], angle) )
    
    def __str__(self):
        return "Matrix: %s\n translation:\n %s\n rotation:\n %s\n" % (self.name,self.translation,self.rotation)

def polygonSurfaceAndCoG( points):
    # from wikipedia
    vertices = points + [ points[0] ]
    sumA = 0.
    sumX = 0.
    sumY = 0.
    for i in range(len(points)):
        thisA = vertices[i][0] * vertices[i+1][1] - vertices[i+1][0] * vertices[i][1]
        sumA += thisA
        sumX += (vertices[i][0]+vertices[i+1][0]) * thisA
        sumY += (vertices[i][1]+vertices[i+1][1]) * thisA
    A = 0.5*sumA
    cogX = sumX/(6.*A)
    cogY = sumY/(6.*A)
    #from math import abs
    return (abs(A),(cogX,cogY))

def polygonSurface( points ):
    return polygonSurfaceAndCoG(points)[0]

def polygonCoG( points ):
    return polygonSurfaceAndCoG(points)[1]

# some global numbers that should probably be put in a dictionary
#tile_width  = 43.47 #mm DO NOT USE
#tile_height = 17.04 #mm DO NOT USE
asic_width  = 14.14 # width (x) in mm
asic_height = 16.6  # height (y) in mm
asic_distance = 0.105 # distance in x between two asics
asic_interval = asic_width + asic_distance # distance in x between start of one asic and start of next asic mm
asic_dist_marker_to_yedge = 0.117 # distance between the marker and the edge of the tile in the y direction
asic_dist_marker_to_xedge = 0.058 # distance between the marker and the edge of the tile in the x direction
# important: tile_width and tile_height are not the total width and
# height of the tile, but of the three asics. for everything below to
# work that is important
tile_width  = 3*asic_width + 2*asic_distance
tile_height = asic_height
PIXEL_SIZE = 0.055
LONG_PIXEL_SIZE = (0.3300 - PIXEL_SIZE)/2
SENSOR_DIST_FIRST_LAST_PIXEL = (3*256-1) * PIXEL_SIZE + 4*(LONG_PIXEL_SIZE-PIXEL_SIZE)
FEHYBRID_WIDTH = 43.25
GBTXHYBRID_WIDTH = 43.90
GBTXHYBRID_HEIGHT = 36.30

# these need to be confirmed from somewhere
CSIDE_GBTXHYBRID_X = 78.7
CSIDE_GBTXHYBRID_Y = -50.3
NSIDE_GBTXHYBRID_X = 78.9
NSIDE_GBTXHYBRID_Y = 4.2

# class to keep the coordinates of a tile. from the position of two
# markers it can compute the transform to go from coordinates in the
# tile, to coordinates in the LHCB frame.
class TileInfo:
    #__slots__ = [ "Name", "FirstMarker", "LastMarker", "Overhangs" ]
    def __init__(self, Name, FirstMarker, LastMarker, Overhangs ) :
        self.Name = Name
        self.FirstMarker = FirstMarker
        self.LastMarker = LastMarker
        self.Overhangs = Overhangs
    def angle(self):
        from math import atan2
        dx = self.LastMarker.x() - self.FirstMarker.x()
        dy = self.LastMarker.y() - self.FirstMarker.y()
        return atan2( dy,dx)
    def transform(self):
        # if we make the origin the midpoint of the middle tile, then it all becomes a lot easier, right?
        # it's just not so easy to do. perhaps derive with multiple transfrorms.

        # this is the transform for the left-bottom marker
        fromMarkerToGlobal =Transform2D()
        fromMarkerToGlobal.setRotation( -self.angle() )
        fromMarkerToGlobal.setTranslation( self.FirstMarker.x(), self.FirstMarker.y() )

        # this is the transform that brings you from the left-bottom corner to the marker
        fromCornerToMarker = Transform2D()
        fromCornerToMarker.setTranslation( -asic_dist_marker_to_xedge, -asic_dist_marker_to_yedge )

        # combine in the total transform
        T = Transform2D( name = self.Name + "Transform")
        T = fromMarkerToGlobal * fromCornerToMarker
        T.name = self.Name + "Transform"
        return T
    def isNSide(self) :
        return "N" in self.Name
    def cornersTileFrame(self):
        return [ Point2D(0,0,self.Name + "_P1"),Point2D(tile_width,0,self.Name + "_P2"),
            Point2D(0,tile_height,self.Name + "_P3"),Point2D(tile_width,tile_height,self.Name + "_P4") ]

# These coordinates are the tile coordinates that come from the module
# drawings. (They used to be in 'global' coordinates, but to make it a
# bit easier to draw the hybrids, I multiplied all y-coordinates with
# -1, and reversed the order of the markers. that puts everything in
# 'robot' coordinates.) Don't change these numbers unless the drawings
# change. If you need to move things globally, change the 'robot'
# transforms.
ctileinfos = [
    TileInfo("CLI",Point2D(-11.069,-41.433),Point2D(18.993,-11.371),
                 Overhangs = [5.0,5.0,5.0]),
    TileInfo("CSO",Point2D(51.273,-1.464),Point2D(21.210,+28.599),
                 Overhangs = [0.0,0.0,0.0] )
    ]

ntileinfos = [
    TileInfo("NSI",Point2D( 11.487,-18.876),Point2D( 41.550, 11.186),
                 Overhangs = [5.0,0.0,0.0] ),
    TileInfo("NLO",Point2D(27.744, 22.066),Point2D(-2.319, 52.128),
                 Overhangs = [0.0,0.0,0.0])]
tileinfos = ctileinfos + ntileinfos

tilelibrary = { tile.Name : tile for tile in tileinfos }

# Some geometry information about the GBTX hybrids. This is not entirely complete yet.

# These are the gbtx corner points in a frame in which the straight
# angle of the hybrid is at (0,0) and all other corners in the first
# quadrant.
gbtxcorners = [(0.,+0.),(0,28.7,0.),(7.6,36.3,),(30.3,36.3,),(43.9,22.7,),(43.9,6.5,),(37.4,0)]
gbtxcenter  = (22.34,20.54) # from the drawing
gbldcenter  = (25.0,6.0)    # measured with a ruler in the GBTX hybrid frame
gbtxcenterInModuleFrame = (58.6,27.1) # measured with the P&P. this is the average of N-side and C-side which are very close
# we can now compute the numbers given above. I don't really like that in x these are 1mm off again.
gbtxcornerInModuleFrameNSide = ( gbtxcenterInModuleFrame[0] + gbtxcenter[1], +gbtxcenterInModuleFrame[1] - gbtxcenter[0] )
gbtxcornerInModuleFrameCSide = ( gbtxcenterInModuleFrame[0] + gbtxcenter[1], -gbtxcenterInModuleFrame[1] - gbtxcenter[0] )
#print( gbtxcornerInModuleFrameNSide, NSIDE_GBTXHYBRID_X, NSIDE_GBTXHYBRID_Y)
#print( gbtxcornerInModuleFrameCSide, CSIDE_GBTXHYBRID_X, CSIDE_GBTXHYBRID_Y)

# These are the nominal transformations to the LHCb frame
gbtxrotation = [[0,-1],[1,0]]
csidegbtxtransform = Transform2D( name = "CSIDEGBTX", rotation = gbtxrotation,
                                      translation = [ [CSIDE_GBTXHYBRID_X],[CSIDE_GBTXHYBRID_Y ] ] )
nsidegbtxtransform = Transform2D( name = "NSIDEGBTX",rotation = gbtxrotation,
                                      translation = [ [NSIDE_GBTXHYBRID_X],[NSIDE_GBTXHYBRID_Y ] ] )

# the FE hybrid corners
fehybridcorners = [ (0,0), (FEHYBRID_WIDTH,0), (FEHYBRID_WIDTH,-16),
                        (FEHYBRID_WIDTH-3,-21), (FEHYBRID_WIDTH-3,-25),
                        (FEHYBRID_WIDTH-9,-31), (4,-31),
                        (0,-24) ]
# FE hybrid origin in tile frame
fehybridoriginintileframe = [ -0.5*(FEHYBRID_WIDTH-SENSOR_DIST_FIRST_LAST_PIXEL), -0.4 ]
    
# the main function
if __name__ == "__main__":
    print("FE hybrid surface: ", polygonSurface( fehybridcorners ))
    print("GBTX hybrid surface: ", polygonSurface( gbtxcorners ))

