# $Id$

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
import time

side = "NHO" # "C" or "N" or "CH" or "NH or CP" or "CHO" or "NHO"

fig_size = plt.rcParams["figure.figsize"]
 
# Prints: [8.0, 6.0]
print("Current size:", fig_size)
 
# Set figure width to 12 and height to 9
fig_size[0] = 8
fig_size[1] = 8
plt.rcParams["figure.figsize"] = fig_size

def getcoordinates(filename, n=2):
	with open(filename) as f:
		x = []
		y = []
		z = []
		for l in f:
			try:
				x.append(float(l.split(',')[-3]))
				y.append(float(l.split(',')[-2]))
				z.append(float(l.split(',')[-1]))
			except:
				pass
	return x[n:],y[n:],z[n:]

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

if side=="C":
	x1,y1,z1 = getcoordinates('CLI_STAR.SRC')
	x2,y2,z2 = getcoordinates('CSO_STAR.SRC')
	ax.scatter(x1,y1,z1, marker='o')
	# ax.plot(x1,y1,z1)	
	ax.scatter(x2,y2,z2, marker='^')
	ax.set_zlim3d(min(z1+z2)-0.5, max(z1+z2)+0.5)  
	ax.invert_zaxis()
	ax.invert_yaxis()
elif side=="CP":
	x1,y1,z1 = getcoordinates('CLI_STAR.SRC')
	x2,y2,z2 = getcoordinates('CSO_STAR.SRC')
	x3,y3,z3 = getcoordinates('CLI_OUTLINE_STAR.SRC')
	x4,y4,z4 = getcoordinates('CSO_OUTLINE_STAR.SRC')	
	ax.scatter(x1,y1,z1, marker='o')
	ax.scatter(x2,y2,z2, marker='^')
	ax.scatter(x3,y3,z3, marker='v')
	ax.scatter(x4,y4,z4, marker='*')	
	ax.set_zlim3d(min(z1+z2)-0.5, max(z1+z2)+0.5)  
	ax.invert_zaxis()
	ax.invert_yaxis()	
elif side=="N":
	x1,y1,z1 = getcoordinates('NSI_STAR.SRC')
	x2,y2,z2 = getcoordinates('NLO_STAR.SRC')
	ax.scatter(x1,y1,z1, marker='o')
	ax.scatter(x2,y2,z2, marker='^')
	ax.set_zlim3d(min(z1+z2)-0.5, max(z1+z2)+0.5)  
	ax.invert_zaxis()
	ax.invert_yaxis()
elif side=="CH":
	x1,y1,z1 = getcoordinates('CLI_FEHYBRID.SRC',2)
	x2,y2,z2 = getcoordinates('CSO_FEHYBRID.SRC',2)
	x4,y4,z4 = getcoordinates('GBTx_STAR_C_side.SRC',2)
	ax.scatter(x1,y1,z1, marker='o')
	ax.scatter(x2,y2,z2, marker='^')
	ax = fig.add_subplot(111)
	ax.scatter(x1,y1, marker='o')
	ax.scatter(x2,y2, marker='^')	
	ax.scatter(x4,y4, marker='*')		
	ax.set_ylim(0,150)
	ax.set_xlim(0,150)	
	# ax.set_zlim3d(min(z1+z2)-0.5, max(z1+z2)+0.5)  
elif side=="NH":
	x1,y1,z1 = getcoordinates('NSI_FEHYBRID.SRC',2)
	x2,y2,z2 = getcoordinates('NLO_FEHYBRID.SRC',2)
	x4,y4,z4 = getcoordinates('GBTx_STAR_N_side.SRC',2)
	ax.scatter(x1,y1,z1, marker='o')
	ax.scatter(x2,y2,z2, marker='^')
	ax = fig.add_subplot(111)
	ax.scatter(x1,y1, marker='o')
	ax.scatter(x2,y2, marker='^')	
	ax.scatter(x4,y4, marker='*')	
	ax.set_ylim(0,150)
	ax.set_xlim(0,150)	
	# ax.set_zlim3d(min(z1+z2)-0.5, max(z1+z2)+0.5)
elif side=="Ctest":
	x1,y1,z1 = getcoordinates('cside_hybrid.src',2)
	# x2,y2,z2 = getcoordinates('cside_hybrid.src',2)
	ax = fig.add_subplot(111)
	ax.scatter(x1,y1, marker='o')
	ax.set_ylim(0,150)
	ax.set_xlim(0,150)
elif side=="combined":
	x1,y1,z1 = getcoordinates('CLI_FEHYBRID.SRC',0)
	x2,y2,z2 = getcoordinates('CSO_FEHYBRID.SRC',0)
	x3,y3,z3 = getcoordinates('cside_hybrid.src',2)
	x4,y4,z4 = getcoordinates('GBTx_STAR_C_side.SRC',2)
	ax = fig.add_subplot(111)
	ax.scatter(x1,y1, marker='o')
	ax.scatter(x2,y2, marker='^')
	ax.scatter(x3,y3, marker='v')
	ax.scatter(x4,y4, marker='*')	
	ax.set_ylim(0,150)
	ax.set_xlim(0,150)

elif side=="combinedN":
	x1,y1,z1 = getcoordinates('NSI_FEHYBRID.SRC',0)
	x2,y2,z2 = getcoordinates('NLO_FEHYBRID.SRC',0)
	x3,y3,z3 = getcoordinates('nside_hybrid.src',2)
	x4,y4,z4 = getcoordinates('GBTx_STAR_N_side.SRC',2)
	ax = fig.add_subplot(111)
	ax.scatter(x1,y1, marker='o')
	ax.scatter(x2,y2, marker='^')
	ax.scatter(x3,y3, marker='v')
	ax.scatter(x4,y4, marker='*')	
	ax.set_ylim(0,150)
	ax.set_xlim(0,150)
elif side=="NHO":
	x1,y1,z1 = getcoordinates('OUTLINE_NSI_FEHYBRID.SRC',2)
	x2,y2,z2 = getcoordinates('OUTLINE_NLO_FEHYBRID.SRC',2)
	# x4,y4,z4 = getcoordinates('GBTx_STAR_N_side.SRC',2)
	ax.scatter(x1,y1,z1, marker='o')
	ax.scatter(x2,y2,z2, marker='^')
	ax = fig.add_subplot(111)
	ax.scatter(x1,y1, marker='o')
	ax.scatter(x2,y2, marker='^')	
	# ax.scatter(x4,y4, marker='*')	
	ax.set_ylim(0,175)
	ax.set_xlim(0,175)
elif side=="CHO":
	x1,y1,z1 = getcoordinates('OUTLINE_CLI_FEHYBRID.SRC',2)
	x2,y2,z2 = getcoordinates('OUTLINE_CSO_FEHYBRID.SRC',2)
	# x4,y4,z4 = getcoordinates('GBTx_STAR_C_side.SRC',2)
	ax.scatter(x1,y1,z1, marker='o')
	ax.scatter(x2,y2,z2, marker='^')
	ax = fig.add_subplot(111)
	ax.scatter(x1,y1, marker='o')
	ax.scatter(x2,y2, marker='^')	
	# ax.scatter(x4,y4, marker='*')		
	ax.set_ylim(0,175)
	ax.set_xlim(0,175)	

	# for i,j in zip(x,y):
 #    # xytext and textcoords are used to offset the labels
 #    ax.annotate("({},{})".format(i, j), xy=(i, j), xytext=(5, 5), textcoords='offset')
 
ax.set_xlabel('X')
ax.set_ylabel('Y')
# ax.set_zlabel('Z')

print(time.asctime())
plt.show()