# $Id$

#import time

# This is the z-position of the surface. (The correction for the
# distance is done below.) Measure this number on the GBTX and FE
# hybrids. They should be in reasonably agreement, within about 100
# micron. The number should also be about the same for n-side and
# c-side. Small differences are not a problem for the gluing, but they
# would mean that surfaces are not in the right place, so we should
# monitor it.

# C-side hybrid alignment
CLI = 45.0
CSO = 45.0
GBTx_C = 45.0
robot_zoffset_cside = (CLI+CSO+GBTx_C)/3;
#robot_zoffset_cside = 45.0 #Default 45.0
xoffset_cside = 0 #new value - old value
yoffset_cside = 0 #new value - old value

# N-side hybrid alignment
NSI = 45.0
NLO = 45.0
GBTx_N = 45.0
robot_zoffset_nside = (NSI +NLO + GBTx_N)/3;
#robot_zoffset_nside = 45.0 #Default 45.0
xoffset_nside = 0 #new value - old value
yoffset_nside = 0 #new value - old value

### default line speed
line_speed = 5 #Default 5

### distance of needle to surface
distance = 0.5

### safe zoffset for outline patterns
zoffset_outlines = 44.0

############################################################################

# FOR THE ALIGNMENT THESE ARE THE ONLY NUMBERS YOU CAN TOUCH!
# Please,no other x and y offsets in the code.
#robot_xoffset_cside = 16.0+35.35-35.58 # 53.9 # 53.6   # x position of LHCb origin in frame of robot
robot_xoffset_cside = xoffset_cside + 16.2
robot_yoffset_cside = yoffset_cside + 85.3
#robot_yoffset_cside = 84.9+73.38-73.38 # 93.35 # 92.7   # y position of LHCb origin in frame of robot
robot_angle_cside = 0  # rotation angle
##############
#robot_xoffset_nside = 16.0+58.13-58.13 # 30.8 # 29.5   # x position of LHCb origin in frame of robot
robot_xoffset_nside = xoffset_nside + 16.2
robot_yoffset_nside = yoffset_nside + 85.5
#robot_yoffset_nside = 84.9+95.94-95.94 # 71.8 # 74.0   # y position of LHCb origin in frame of robot
robot_angle_nside = 0  # rotation angle

#Print the alignment in power shell
print()
print ("Line speed: "+"{:.2f}".format(line_speed))
print ("Distance needle tip to hybrid: "+"{:.2f}".format(distance))
print()
print ("Z-offset C-side: "+"{:.2f}".format(robot_zoffset_cside))
print ("Robot alignment C-side: "+"{:.2f}".format(robot_zoffset_cside - distance))
print ("X-offset C-side: "+"{:.2f}".format(robot_xoffset_cside))
print ("Y-offset C-side: "+"{:.2f}".format(robot_yoffset_cside))
print ("Rotation angle C-side: "+"{:.2f}".format(robot_angle_cside))
print()
print ("Z-offset N-side: "+"{:.2f}".format(robot_zoffset_nside))
print ("Robot alignment N-side: "+"{:.2f}".format(robot_zoffset_nside - distance))
print ("X-offset N-side: "+"{:.2f}".format(robot_xoffset_nside))
print ("Y-offset N-side: "+"{:.2f}".format(robot_yoffset_nside))
print ("Rotation angle N-side: "+"{:.2f}".format(robot_angle_nside))
print ()

# write the header of the gluing file with common parameters
def writeheader( outputfile, programname, linespeed=20 ):
    Zclearance = 5. # mm
    headtime = 0.1 #sec
    tailtime = 0.0 #sec
    nodetime = 0.0 #sec
    taillength = 1.0 #mm
    pointtime = 0.1 #sec
    pointtailtime = 0.25 #sec
    outputfile.write("Program Name, %s\n" % programname)
    outputfile.write(f"Line Speed, {linespeed:3.1f}\n")
    outputfile.write(f"Line Dispense Setup,{headtime:4.2f},{tailtime:5.2f},{nodetime:5.2f},{taillength:5.2f}\n")
    outputfile.write(f"Point Dispense Setup,{pointtime:4.2f},{pointtailtime:5.2f}\n")    
    outputfile.write("Dispense End Setup, 5.0, 2.5, 2.0\n")
    outputfile.write(f"Z Clearance,{Zclearance:3.1f}, 1\n")

# This function creates a 'SRC' file for a single FE hybrid
def writehybridpattern( outputfile, transform, zoffset ) :
    # the origin is the first marker of the tile. the tile is slightly narrower
    # than the hybrid. we stay at a distance of 2 mm from the side. at some
    # point the lines lengths become a bit more complicated.
    
    from geometry import Point2D
    from geometry import SENSOR_DIST_FIRST_LAST_PIXEL, FEHYBRID_WIDTH
    x0 = -0.5*(FEHYBRID_WIDTH - SENSOR_DIST_FIRST_LAST_PIXEL)
    DISTANCE_TO_HYBRID_EDGE = 1.0 
    XMIN = x0 + DISTANCE_TO_HYBRID_EDGE
    XMAX = x0 + FEHYBRID_WIDTH - DISTANCE_TO_HYBRID_EDGE

    # why is the second line shorter than the first???
    lineypositions =  [ -3.0, -8.65, -20.07, -26.95 ]
    # swap the order such that the 'missing' bit of line at the beginning is not a problem
    lineypositions =  [ -8.7, -3.0, -14.4, -20.0, -26.95 ]
    linex1positions = [ XMIN, XMIN, XMIN, XMIN , XMIN + 2 ]
    linex2positions = [ XMAX, XMAX, XMAX, XMAX - 3 , XMAX - 6 ]
    for (x1,x2,y) in zip( linex1positions, linex2positions, lineypositions ):
        x1p = transform * Point2D( x1,y) 
        x2p = transform * Point2D( x2,y)
        outputfile.write(f"Line Start,    {x1p.x():8.3f}, {x1p.y():8.3f}, {zoffset:8.3f}\n")
        outputfile.write(f"Line End,      {x2p.x():8.3f}, {x2p.y():8.3f}, {zoffset:8.3f}\n")
    
# This function creates a 'SRC' file for a single FE hybrid
def writehybridoutline( outputfile, transform, zoffset = 44) :
    from geometry import fehybridcorners, fehybridoriginintileframe, Point2D
    # add a point for the determining the z-offset
    points = fehybridcorners + [ (21.6,-3.0) ]
    # transform them to the right frame
    for c in points:
        p = transform*Point2D( fehybridoriginintileframe[0] + c[0], fehybridoriginintileframe[1] + c[1]) 
        outputfile.write(f"Dispense Dot,    {p.x():8.3f}, {p.y():8.3f}, {zoffset:8.3f}\n")

def writegbtxpattern(outputfile, transform, zoffset, linespeed):

    # the origin is the lower left corner. Manchester draws vertical lines, so why not do the same thing :-)
    NUM_LINES = 9
    DISTANCE_TO_HYBRID_EDGE = 2.0
    from geometry import GBTXHYBRID_WIDTH, GBTXHYBRID_HEIGHT
    LINE_DIST = (GBTXHYBRID_WIDTH - 2*DISTANCE_TO_HYBRID_EDGE)/(NUM_LINES-1)
    YMIN = DISTANCE_TO_HYBRID_EDGE
    YMAX = GBTXHYBRID_HEIGHT - DISTANCE_TO_HYBRID_EDGE
    # I still find these lines a little close to the edge, especially for the shorter lines
    y1positions = 8*[YMIN] + [YMIN + 5 ] 
    y2positions = [YMAX - 6, YMAX - 1 ] + 4*[YMAX] + [ YMAX - 2, YMAX - 7, YMAX - 12 ]
    from geometry import Point2D
    index = 0
    for y1, y2, index in zip( y1positions, y2positions, range(NUM_LINES)):
        x = DISTANCE_TO_HYBRID_EDGE + index*LINE_DIST
        if index%2 == 0:
            p1 = transform * Point2D( x, y1 )
            p2 = transform * Point2D( x, y2 )
        else:
            p1 = transform * Point2D( x, y2 )
            p2 = transform * Point2D( x, y1 )
        outputfile.write(f"Line Start,  {p1.x():8.3f},{p1.y():8.3f},{zoffset:8.3f}\n")
        outputfile.write(f"Line End,    {p2.x():8.3f},{p2.y():8.3f},{zoffset:8.3f}\n")


def writegbtxstarpattern( outputfile, transform, zoffset, usecog = True ):
    # This creates a star pattern for the GBX hybrid. The distance
    # between the lines is chosen such that between each two lines the
    # surface is the same. The default is to use the center-of-grav

    from geometry import gbtxcorners, gbtxcenter, Point2D
    from math import sqrt

    # compute the surface center-of-gravity of the polygon
    from geometry import polygonSurfaceAndCoG
    (A,center) = polygonSurfaceAndCoG( gbtxcorners )
    Nlines = 20
    deltaA = A/Nlines
    distancetoedge = 2

    # now we walk along the outer side of the polygon, in steps such that each triangle has the same surface
    closedcorners = gbtxcorners + [ gbtxcorners[0] ]
    nextcornerindex = 1
    currentpos = closedcorners[0]
    nextcorner = closedcorners[1]
    if not usecog: center = gbtxcenter
    
    # write the first line, then do the rest
    Nlinesprocessed = 0
    restdeltaA = deltaA
    while Nlinesprocessed < Nlines:
        # need an escape for the last line
        drawnextline = Nlinesprocessed == Nlines-1
        mu           = 1
        if not drawnextline:
            # mu represents the fraction of the next triangle
            nextcorner = closedcorners[nextcornerindex]
            thisA  = 0.5 * abs( (nextcorner[1] - currentpos[1]) * ( currentpos[0] - center[0] ) -
                                    (nextcorner[0] - currentpos[0]) * ( currentpos[1] - center[1] ) )
            drawnextline = thisA >= restdeltaA
            mu = restdeltaA / thisA
        if drawnextline:
            nextpos = ( currentpos[0] + mu*(nextcorner[0] - currentpos[0]),
                            currentpos[1] + mu*(nextcorner[1] - currentpos[1]) )
            dx = nextpos[0] - center[0]
            dy = nextpos[1] - center[1]
            thisL = sqrt(dx*dx+dy*dy)
            scale = (thisL - distancetoedge)/thisL
            # now need the coordinates of the line, then shorten it.
            p2 = transform * Point2D(  center[0], center[1] )
            p1 = transform * Point2D(  center[0]+dx*scale, center[1] +dy*scale )
            outputfile.write(f"Line Start,  {p1.x():8.3f},{p1.y():8.3f},{zoffset:8.3f}\n")
            outputfile.write(f"Line End,    {p2.x():8.3f},{p2.y():8.3f},{zoffset:8.3f}\n")
            currentpos = nextpos
            restdeltaA = deltaA
            Nlinesprocessed += 1
        else:
            currentpos = nextcorner
            restdeltaA -= thisA
            nextcornerindex += 1
     
def writegbtxoutline(outputfile, transform, zoffset = 44.):
    # for now just two dots, on the lower. these are now in the wrong frame.
    from geometry import gbtxcorners, gbtxcenter, Point2D
    points = gbtxcorners + [ gbtxcenter ]
    for c in points:
        p = transform * Point2D(c[0],c[1])
        outputfile.write(f"Dispense Dot,    {p.x():8.3f}, {p.y():8.3f}, {zoffset:8.3f}\n")

def writefile(filename, patterns, linespeed ):
    outputfile = open(filename + ".SRC", "w")
    writeheader( outputfile, programname=filename, linespeed=linespeed )
    for (index,(label,function,args)) in enumerate(patterns):
        outputfile.write(f"Label,{label}\n")
        function(outputfile=outputfile, **args)
    outputfile.write("End Program\n")
    outputfile.close()
    
# transformations to get from the frame in which the marker positions are defined to the frame of the glue robot
from math import cos, sin
from geometry import Transform2D
robot_cos_cside = cos( robot_angle_cside )
robot_sin_cside = sin( robot_angle_cside )
csideRobotTransform = Transform2D( name = "CSideRobotTransform",
                                   translation = [[robot_xoffset_cside],[robot_yoffset_cside]],
                                   rotation = [[robot_cos_cside,-robot_sin_cside],[robot_sin_cside,robot_cos_cside]] )
robot_cos_nside = cos( robot_angle_nside )
robot_sin_nside = sin( robot_angle_nside )
nsideRobotTransform = Transform2D( name = "NSideRobotTransform",
                                   translation = [[robot_xoffset_nside],[robot_yoffset_nside]],
                                   rotation = [[robot_cos_nside,-robot_sin_nside],[robot_sin_nside,robot_cos_nside]] )

if __name__ == "__main__":
    from geometry import ctileinfos,ntileinfos
    zoffset_nside = robot_zoffset_nside - distance
    zoffset_cside = robot_zoffset_cside - distance
    from geometry import csidegbtxtransform,nsidegbtxtransform

    for (side,tiles,zoffset,robottransform,gbtxtransform) in [
            ("CSide",ctileinfos,zoffset_cside,csideRobotTransform,csidegbtxtransform),
            ("NSide",ntileinfos,zoffset_nside,nsideRobotTransform,nsidegbtxtransform)]:
            for tile in tiles:
                tiletransform = robottransform * tile.transform()
                for (filename,patterns) in [
                        (f"{tile.Name}_FEHYBRID",[(tile.Name,writehybridpattern,{"zoffset": zoffset,"transform": tiletransform, })]),
                        (f"OUTLINE_{tile.Name}_FEHYBRID",[(tile.Name,writehybridoutline,{"zoffset": zoffset_outlines, "transform": tiletransform})]) ]:
                        writefile( filename=filename, linespeed=line_speed, patterns=patterns )
                    
            for (filename,patterns) in [
                (f"OUTLINE_{side}_GBTX",[ ("GTBX",writegbtxoutline,{"zoffset": zoffset_outlines, "transform":robottransform * gbtxtransform} )]),
                (f"{side}_GBTX",[ ("GTBX",writegbtxstarpattern,{"zoffset" : zoffset,"transform" : robottransform * gbtxtransform}) ] ) ]:
                    writefile( filename=filename, linespeed=line_speed, patterns=patterns )

            # new: also write a single file with all three patterns
            writefile( filename=f"{side}_HYBRIDS", linespeed=line_speed,
                        patterns= [
                                (tiles[0].Name,writehybridpattern,{"zoffset": zoffset,"transform": robottransform * tiles[0].transform()}),
                                (tiles[1].Name,writehybridpattern,{"zoffset": zoffset,"transform": robottransform * tiles[1].transform()}),
                                ("GBTX",writegbtxstarpattern,{"zoffset": zoffset,"transform": robottransform * gbtxtransform})] )

    import time
    print(time.asctime())

def drawpatterns(directory="."):
    import matplotlib.pyplot as plt
    fig, axs = plt.subplots(1,2)
    fig.set_size_inches(12,6)
    fig.subplots_adjust(left=0.05,right=0.98,bottom=0.05,top=0.95,wspace = 0.2,hspace=0.2)
    from visualization import drawPattern, drawCSideTiles, drawNSideTiles
    pattern = "FEHYBRID"
    drawCSideTiles(axs[0],"C-side", transform = csideRobotTransform)
    drawPattern(axs[0], directory + "/CLI_%s.SRC" % pattern)
    drawPattern(axs[0], directory + "/CSO_%s.SRC" % pattern)
    drawPattern(axs[0], directory + "/CSide_GBTX.SRC")
    #drawPattern(axs[0], directory + "/CSide_GBTX_STAR.SRC")
    outlinecolor = 4
    drawPattern(axs[0], directory + "/OUTLINE_CLI_FEHYBRID.SRC", index =outlinecolor )
    drawPattern(axs[0], directory + "/OUTLINE_CSO_FEHyBRID.SRC", index = outlinecolor)
    drawPattern(axs[0], directory + "/OUTLINE_CSide_GBTX.SRC", index = outlinecolor)
    drawNSideTiles(axs[1],"N-side", transform = nsideRobotTransform)
    drawPattern(axs[1],directory + "/NSI_%s.SRC" % pattern)
    drawPattern(axs[1],directory + "/NLO_%s.SRC" % pattern)
    drawPattern(axs[1], directory + "/NSide_GBTX.SRC")
    drawPattern(axs[1], directory + "/OUTLINE_NSI_FEHYBRID.SRC", index = outlinecolor)
    drawPattern(axs[1], directory + "/OUTLINE_NLO_FEHyBRID.SRC", index = outlinecolor)
    drawPattern(axs[1], directory + "/OUTLINE_NSide_GBTX.SRC", index = outlinecolor)
    axs[0].set_aspect('equal')
    axs[1].set_aspect('equal')
    axs[0].set_title("c-side %s" % pattern)
    axs[1].set_title("n-side %s" % pattern)
    fig.savefig(directory + '/patterns_%s.pdf' % pattern)
    plt.show()
