# analysis-scripts

Repository for all the scripts used in the module production and testing

## Electrical Tests
### quick_display.py

Original author: Victor Coco

Usage:

     python -i quick_display /The/path/to/you/equalisation/folder PLOTS

where `PLOTS` can be `NOISE`, `IV`, `EQ`, `PRB`