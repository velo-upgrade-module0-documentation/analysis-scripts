#!/bin/bash
  
# Define location of cernbox on this machine
CERNBOX=/home/velo/cernbox/Production

# Check for just one argument
if [ $# -ne 1 ]
then
    echo "Please enter the name of the module you wish to compress data for to upload to the DB"
    exit
fi

# Use argment to check for module
MOD=$1
echo "Compressing data for module $MOD"
BASE_DIR=${CERNBOX}/${MOD}

# Check if base directory exists
if [ ! -d ${BASE_DIR} ]
then 
    echo "Directory $BASE_DIR does not exist, exiting"
    exit
fi

# Compress relevant data

# Tile metrology

# tar -cvf 20201027_N016_thickness.tar CLISensorSurface.csv CSideSubstrateSurface.csv CSOSensorSurface.csv NLOSensorSurface.csv NSideSubstrateSurface.csv NSISensorSurface.csv

# Hybrid glue  atterns
cd ${BASE_DIR}/hybrid_glueing/glue_files
tar -cvf cside_patterns_${MOD}.tar *C*.SRC
tar -cvf nside_patterns_${MOD}.tar *N*.SRC
cd -

# Hybrid photographs
cd ${BASE_DIR}/hybrid_glueing/photographs_c_side
tar -cvf cside_hybrid_photographs_${MOD}.tar *
cd -
cd ${BASE_DIR}/hybrid_glueing/photographs_n_side
tar -cvf nside_hybrid_photographs_${MOD}.tar *
cd -

