
#!/usr/bin/python

import os,glob,shutil,re

import matplotlib.pyplot as plt
import matplotlib.dates as mdates


import pandas as pd

from optparse import OptionParser
parser = OptionParser(usage = "usage: %prog [options] <inputfiles>")
parser.add_option("-v","--verbose", action="store_true",help="very verbose mode")
(opts, args) = parser.parse_args()



class VoltageMeasurement:
    def __init__(self, modulename, asicname, Va, Vd, datestring):
        self.modulename = modulename
        self.asicname = asicname
        self.Vanalog = Va
        self.Vdigitial = Vd
        self.datestring = datestring

def readline( modulename, line ):
    # need to so dome pattern matching
    #2021-05-12 10:58:54 VP0-0  0.0000 -0.0242 0.0000 -172.22 2 -1
    #markerpattern = re.compile(".*?(?P<coord>(X|Y)) Value_(?P<name>(ASIC|Sensor|Asic) Marker \d{1,2})")
    
    matches = linepattern.match(line)
    if matches:
        print(matches.group('date'), matches.group('ASIC'), matches.group('data'))
    

def main():
    files = args
    linepattern = re.compile("(?P<date>(\d{4}-\d{2}-\d{2} \d\d:\d\d:\d\d)) (?P<ASIC>(VP\d-\d)) (?P<ASICID>(\d{8})) (?P<data>(.*)?)")
    modulenamepattern = re.compile(".*?(?P<name>N\d\d\d).*?")
    measurements = []
    df = pd.DataFrame()
    for filename in files:
        thisdf = pd.read_csv(filename,sep=' ',index_col=False,skiprows=3,names=['date','time','ASIC','VPID','volt_ana','volt_dig','volt_band_gap','degree_band_gap','power_mode','fsm_state'],
                                 parse_dates=['date'])
        # some sanity check because some data seems corrupt
        indexer = thisdf['ASIC'].apply(lambda x: isinstance(x,str))
        thisdf = thisdf.loc[indexer]        
        indexer = thisdf['power_mode'].apply(lambda x:x==2)
        thisdf = thisdf.loc[indexer]
        
                
        # add a column with the modulename
        result = modulenamepattern.search( filename )
        if result: modulename = result.group('name')
        else: modulename = "unknown"
        thisdf['modname'] = len(thisdf)*[modulename]
        print(filename,len(thisdf))
        
        #print(thisdf)
        if df.empty: df = thisdf
        else: df = df.append(thisdf,ignore_index=True)

    print(df.head())
    print("total number of entries: %d" %(len(df)))

    fig, axes = plt.subplots(2)
    fig.set_size_inches(10,8)
    fig.subplots_adjust(left=0.1,right=0.85,bottom=0.05,top=0.95,wspace = 0.3,hspace=0.3)
    fig.autofmt_xdate()
    
    #df.plot(x='date',y='volt_ana')
    #df.plot(x='date',y='power_mode')
    #df.pivot_table(index='date',columns='ASIC',values='volt_ana',aggfunc='sum').plot()
    for axis,yval in zip(axes,['volt_ana','volt_dig']):
        df.pivot_table(index='date',columns='ASIC',values=yval).plot(ylim=(1.1,1.4),style='.',ax=axis,rot=45,x_compat=True)
        axis.set_title(yval)
        axis.legend(bbox_to_anchor=(1,1), loc="upper left")

    #fig = ax.get_figure()
    fig.savefig('voltages.pdf')
    fig.savefig('voltages.png')
    #for m in measurements: print(m.modulename, m.asicname, m.datestring)

    # sort by ASICID

    #times = [ m.datestring for m in measurements ]
    #Vana  = [ m.Vanalog for m in measurements ]

   
    #plt.plot(times, Vana)
    #xfmt = mdates.DateFormatter('%d-%m-%y %H:%M')
    #xfmt = mdates.DateFormatter('%d-%m-%y %H:%M')
    #ax.xaxis.set_major_formatter(xfmt)
    #ax.set_ylim([1.0,1.4])
    
    plt.show()

    
main()
