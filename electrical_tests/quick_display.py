'''
Hi Igor, Stefano,

here is the piece of code with the remaining plots.

Usage change a little bit to let you select which one you want: python -i quick_display /The/path/to/you/equalisation/folder PLOTS

where PLOTS can be NOISE, IV, EQ, PRBS

It is also good to check if you file and file structure are DB compatible:
- it assumes the PRBS file is named PRBS.log (which I will assume in the DB)
- it assumes the IV files are in a IVCurves folder and are names IVCurve_NLO.dat ,IVCurve_CSI.dat etc...

noise and equalisation filename and stucture are created by WinCC so I assume they won't change.

Feel free to modify as you wish, for the DB version it has access to all info so there will be more infos in the labels and so on.

Cheers,
Victor
'''

# python -i quick_display /The/path/to/you/equalisation/folder PLOTS
# where PLOTS can be NOISE, IV, EQ, PRBS

import numpy as np
import matplotlib.pyplot as plt
import os, fnmatch, re
import sys

folder = sys.argv[1]
tile_map = {'CLI': 'VP0','CSO': 'VP3','NLO':'VP1','NSI':'VP2'}

if sys.argv[2] == 'NOISE':
    # Prepare the noise histograms and 2d maps
    _nhist_plt, _nhist_ax = plt.subplots(4, 3, figsize=(20, 15))
    _noise_plt, _noise_ax = plt.subplots(4, 1, figsize=(12, 15))
    # Loop over the tiles
    for i_t, name_tile in enumerate(['CLI', 'CSO', 'NLO', 'NSI']):
        noise = np.zeros((256 * 3, 256))
        for i_a, name_asic in enumerate(['Asic0', 'Asic1', 'Asic2']):
            # Get the data
            noise[i_a * 256: (i_a + 1) * 256, :] += (np.loadtxt(
                '%s/%s_%s_noise_Trim0.txt' % (folder, tile_map[name_tile], name_asic), delimiter=',').T + np.loadtxt(
                '%s/%s_%s_noise_TrimF.txt' % (folder, tile_map[name_tile], name_asic), delimiter=',').T) / 2.
            # Plot the histos
            Y, X = np.histogram(noise[i_a * 256:(i_a + 1) * 256].flatten(), bins=np.arange(4., 10., 0.1))
            # Make it ~ nice
            _nhist_ax[i_t, i_a].step(X[:-1], Y, color='k', linewidth=3, label='%s %s : %.2f$\pm %.3f DAC $' % (
                name_tile, name_asic, np.nanmean(noise[i_a * 256:(i_a + 1) * 256]),
                np.nanstd(noise[i_a * 256:(i_a + 1) * 256])))
            _nhist_ax[i_t, i_a].set_yscale('log')
            _nhist_ax[i_t, i_a].set_xlabel('DAC count')
            _nhist_ax[i_t, i_a].set_ylabel('$N_{hits}')
            _nhist_ax[i_t, i_a].legend()
        # Plot the 2d Maps
        X, Y = np.meshgrid(np.arange(256 * 3), np.arange(256))
        im = _noise_ax[i_t].pcolor(X.T, Y.T, noise, vmin=4, vmax=8)
        _noise_ax[i_t].set_xlabel('Column')
        _noise_ax[i_t].set_ylabel('Row')
        _noise_ax[i_t].set_title("%s in folder %s" % (name_tile, folder))
        _noise_plt.colorbar(im, ax=_noise_ax[i_t])
    plt.show()

if sys.argv[2] == 'IV':
    # Prepare the noise histograms and 2d maps
    _niv_plt, _niv_ax = plt.subplots(4, 1, figsize=(12, 15))
    # Loop over the tiles
    for i_t, name_tile in enumerate(['CLI', 'CSO', 'NLO', 'NSI']):
        try:
            # Get the data
            iv_curve = np.loadtxt("%s/IVCurves/IVCurve_%s.dat"%(folder,name_tile), dtype=float, comments='#', delimiter='\t')
            v,i,t = np.abs(iv_curve[ :, 0]),np.abs(iv_curve[ :, 1]) * 1e9, np.mean(iv_curve[ :, 3])
            _niv_ax[i_t].plot(v,i,label='%s %.0f $^{\circ}C$' % (name_tile,t), linewidth=3)
            _niv_ax[i_t].set_xlim(0, 1000)
            _niv_ax[i_t].set_xlabel('Voltage [V]')
            _niv_ax[i_t].set_ylim(1., 1.e4)
            _niv_ax[i_t].set_ylabel('Current [nA]')
            _niv_ax[i_t].set_yscale('log')
            _niv_ax[i_t].legend()
        except Exception as e:
            print('Problem with %s IV'%name_tile,e)
    plt.show()

if sys.argv[2] == 'PRBS':
    # Extract the data
    ff = open('%s/PRBS.log'%folder,'rb')
    prbs = np.ones((20,3))*np.nan
    ii = 0
    for line in ff.readlines():
        try:
            lin = line.decode('UTF-8')
        except:
            print(line)
        if 'Link_' not in lin: continue
        hh = lin.split('\n')[0].split('\r')[0].split('\t')[1:4]
        for kk,hhh in enumerate(hh):
            val = float(hhh)
            prbs[ii,kk]=val
        ii+=1
    # Prepare figure
    plt.subplots(2,1,figsize=(7,8))
    plt.subplot(2,1,1)
    plt.bar(np.arange(1,21),prbs[:,0],label='N bits tested')
    plt.bar(np.arange(1,21),prbs[:,1],label='N errors')
    plt.yscale('log')
    plt.xlabel('Link#')
    plt.ylabel('N')
    plt.ylim(1.,2.e14)
    plt.legend()
    plt.subplot(2,1,2)
    plt.bar(np.arange(1,21),prbs[:,2])
    plt.yscale('log')
    plt.xlabel('Link#')
    plt.ylabel('Error rate')
    plt.ylim(5.e-15,1.)
    plt.show()

if sys.argv[2] == 'EQ':
    # Prepare the noise histograms and 2d maps
    _nhist_plt, _nhist_ax = plt.subplots(4, 3, figsize=(20, 15))
    _noise_plt, _noise_ax = plt.subplots(4, 1, figsize=(12, 15))
    # Loop over the tiles
    for i_t, name_tile in enumerate(['CLI', 'CSO', 'NLO', 'NSI']):
        mask = np.zeros((256 * 3, 256))
        for i_a, name_asic in enumerate(['Asic0', 'Asic1', 'Asic2']):
            # Get the data
            thresholds = np.zeros((3, 256*256))
            mask[i_a * 256: (i_a + 1) * 256, :] += np.loadtxt( '%s/%s_%s_maskFile.txt' % (folder, tile_map[name_tile], name_asic), delimiter=',').T
            thresholds[0, :] += np.loadtxt( '%s/%s_%s_threshold_0.txt' % (folder, tile_map[name_tile], name_asic), delimiter=',').flatten()
            thresholds[1, :] += np.loadtxt( '%s/%s_%s_threshold_F.txt' % (folder, tile_map[name_tile], name_asic), delimiter=',').flatten()
            thresholds[2, :] += np.loadtxt( '%s/%s_%s_threshold_Exp.txt' % (folder, tile_map[name_tile], name_asic), delimiter=',').flatten()
            # Plot the histos
            _nhist_ax[i_t, i_a].hist(thresholds[0,:], bins=np.arange(1000., 1800., 5), label = '%s thresholds @ 0 Trim', color='r',histtype = 'step')
            _nhist_ax[i_t, i_a].hist(thresholds[1,:], bins=np.arange(1000., 1800., 5),  label = '%s thresholds @ F Trim', color = 'b',histtype = 'step')
            _nhist_ax[i_t, i_a].hist(thresholds[2,:], bins=np.arange(1000., 1800., 5),  label = '%s expected eq. thresholds', color = 'k',histtype = 'step')
            # Make it ~ nice
            _nhist_ax[i_t, i_a].set_yscale('log')
            _nhist_ax[i_t, i_a].set_xlabel('DAC count')
            _nhist_ax[i_t, i_a].set_ylabel('$N_{pixels}')
            _nhist_ax[i_t, i_a].legend()
        # Plot the 2d Maps
        X, Y = np.meshgrid(np.arange(256 * 3), np.arange(256))
        im = _noise_ax[i_t].pcolor(X.T, Y.T, mask, vmin=0, vmax=1)
        _noise_ax[i_t].set_xlabel('Column')
        _noise_ax[i_t].set_ylabel('Row')
        _noise_ax[i_t].set_title("%s in folder %s" % (name_tile, folder))
    plt.show()
